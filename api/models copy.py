from django.db import models
from django.contrib.auth.models import User

# Create your models here.


   
class Book(models.Model):
    name = models.CharField(max_length=30)
    front_cover_photo = models.ForeignKey('Photo', null = True, blank = True, related_name='front_cover_photo')
    back_cover_photo = models.ForeignKey('Photo', null = True, blank = True, related_name='back_cover_photo')
    book_title = models.CharField(max_length=20, null = False, blank = False)
#    book_name = models.CharField(max_length=20, null = True, blank = True)
    filename = models.CharField(max_length=20, null = True, blank = True)
    entries = models.ManyToManyField('Entry', through='BookEntries', blank = True, null = True)
#    boxentry = models.ManyToManyField('BoxEntries')
    
    def add_entry(self, entry):
        self.entries.add(entry)
    
    def __unicode__(self):
        return self.name



class Box(models.Model):
    name = models.CharField(max_length=30)
    cover_photo = models.FileField(upload_to='photo/')
    date_of_birth = models.DateField()
    box_type1 = models.CharField(max_length=20)
    box_type2 = models.CharField(max_length=20, null = True, blank = True)
    entries = models.ManyToManyField('Entry', through='BoxEntries')
#    boxentry = models.ManyToManyField('BoxEntries')
    
    def add_entry(self, entry):
        self.entries.add(entry)
    
    def __unicode__(self):
        return self.name


class BoxEntries(models.Model):
    box = models.ForeignKey('Box')
    entry = models.ForeignKey('Entry')
    approved = models.BooleanField(default = False)
    
    class Meta:
        verbose_name_plural = "box entries"

class BookEntries(models.Model):
    book = models.ForeignKey('Book')
    entry = models.ForeignKey('Entry')
    
    class Meta:
        verbose_name_plural = "book entries"
    
    
class UserProfile(models.Model):
    user = models.OneToOneField(User)
    facebook_id = models.CharField(max_length=30, blank = True, null = True)
    facebook_username = models.CharField(max_length=100, blank = True, null = True)
    facebook_email = models.CharField(max_length=100, blank = True, null = True)
    boxes = models.ManyToManyField('Box', related_name ="users", blank = True, null = True)
    books = models.ManyToManyField('Book', related_name ="users", blank = True, null = True)
    events = models.ManyToManyField('Event', blank = True, null = True)
    friends = models.ManyToManyField('UserProfile', blank = True, null = True)
    notifications = models.ManyToManyField('Notifications', blank = True, null = True)
    access_token = models.CharField(max_length=300, null = True, blank = True)
    facebook_registered = models.BooleanField()
    treasured_registered = models.BooleanField()
    registration_token = models.CharField(max_length=40, null = True, blank = True) 
    password_token = models.CharField(max_length=40, null = True, blank = True)
    space_used = models.FloatField(default = 0)
    usage_plan = models.ForeignKey('UsagePlans', null = True, blank = True)
    customer_id = models.BigIntegerField(null = True, blank = True)
    
    def __unicode__(self):
        print self.user
        return self.user.username    
    
    def save(self, *args, **kwargs):
        if self.usage_plan is None and UsagePlans.objects.filter(name = "Free").count() == 1:
            self.usage_plan = UsagePlans.objects.get(name = "Free")
        super(UserProfile, self).save(*args, **kwargs)
    
    
class Entry(models.Model):
    ENTRY_TYPES = (
                   ('PH', 'Photo'),
                   ('EV', 'Event'),
                   ('DN', 'Diary Note')
                  )
    
    type = models.CharField(max_length=3, choices = ENTRY_TYPES)
    title = models.CharField(max_length=30)
    date = models.DateField()
    is_with = models.CharField(max_length=100, blank=True)
    latitude = models.FloatField()
    longitude = models.FloatField()
    location_desc = models.CharField(max_length=50, blank=True)
    
    def __unicode__(self):
        return self.title  
    
    
    class Meta:
        verbose_name_plural = "entries"    
    
    def getSubClass(self):
        if self.type == 'PH':
            return Photo.objects.filter(id = self.id).values('date', 'id', 'is_with', 'location_desc', 'photo_caption', 'photo_pic', 'title', 'type')[0]
        elif self.type == 'EV':
            return Event.objects.filter(id = self.id).values('date', 'id', 'is_with', 'location_desc', 'title', 'type')[0]
        elif self.type == 'DN':
            return Note.objects.filter(id = self.id).values('date', 'id', 'is_with', 'location_desc', 'note_text', 'title', 'type')[0]
        else:
            return None

class Event(Entry):
#    entry = models.ForeignKey('Entry', related_name="entry_event")
    cover_photo = models.FileField(upload_to='photo/')
    entries = models.ManyToManyField('Entry', related_name="event_entries", blank=True, null=True)
    
class Note(Entry):
#    entry = models.ForeignKey('Entry', related_name="entry_note")
    note_text = models.CharField(max_length=500)
    
class Photo(Entry):
#    entry = models.ForeignKey('Entry', related_name="entry_photo")
    photo_pic = models.CharField(max_length=100, blank=True)
    photo_caption = models.CharField(max_length=100, blank=True)
    photo_full_size = models.IntegerField(default = 0)
    photo_file = models.FileField(upload_to='photo/')
#    def __unicode__(self):
#        return self.photo
    
    
class Collaborators(models.Model):
    user = models.ManyToManyField('UserProfile')
    box = models.ManyToManyField('Box')
    can_read = models.BooleanField()
    can_write = models.BooleanField()
    is_owner = models.BooleanField()
    req_approval = models.BooleanField()
    
    class Meta:
        verbose_name_plural = "collaborators"
    
class Request(models.Model):
    user = models.OneToOneField('UserProfile')
    entry = models.OneToOneField('Entry')
    date = models.DateField()
    box = models.OneToOneField('Box')
    book = models.OneToOneField('Book')
   
class Notifications(models.Model):
    table = models.CharField(max_length=100, blank=True, null=True)
    table_id = models.IntegerField(blank=True, null=True)
    date_update = models.DateTimeField(auto_now = True, blank=True, null=True)
    left_data = models.CharField(max_length=100, blank=True, null=True)
    right_data = models.CharField(max_length=100, blank=True, null=True)
    seen = models.BooleanField()
    type = models.CharField(max_length=100)
    def __unicode__(self):
        return self.type
    
class KeyValue(models.Model):
    key = models.CharField(max_length=100)
    value = models.CharField(max_length=50000)
    
    def __unicode__(self):
        return self.key
    
class UsagePlans(models.Model):
    name = models.CharField(max_length=100)
    max_size = models.IntegerField()
    price = models.IntegerField()
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        verbose_name_plural = "usage plans"
    