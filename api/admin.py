from api.models import *
from django.contrib import admin

class ApiAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['username']}),
    ]
    

admin.site.register(UserProfile)
admin.site.register(Company)
admin.site.register(User_Device)
admin.site.register(UserGroup)
admin.site.register(Country)
admin.site.register(AirCraft)
admin.site.register(Battery)
admin.site.register(CompanyType)
admin.site.register(Job)
admin.site.register(Site)
admin.site.register(Flight)
admin.site.register(FlightCrew)
admin.site.register(PreFlightSurvey)
admin.site.register(OnSiteSurvey)
admin.site.register(Staff)
admin.site.register(Person)
admin.site.register(Log)
admin.site.register(BlackBox)
admin.site.register(Role)
admin.site.register(FlightBattery)

