from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm, Form
from api.models import UserProfile, Company, AirCraft, Battery, CompanyType, Job, Site, Flight, PreFlightSurvey, OnSiteSurvey, Person, Staff, Role,  User_Device, UserGroup, Country, Log, BlackBox

class DisableCSRF(object):

    def process_request(self, request):

        setattr(request, '_dont_enforce_csrf_checks', True)
    

class AddUserProfileForm(ModelForm):
    class Meta:
        model = UserProfile
        exclude= ('user',
                  'access_token',
                  'qtq_registered',
                  'registration_token',
                  'password_token',
                  'customer_id',
                  'tour')
#         exclude= ('user',
#                   'user_group',
#                   'access_token',
#                   'qtq_registered',
#                   'registration_token',
#                   'password_token',
#                   'customer_id',
#                   'address1',
#                   'town_city', 
#                   'county_state',
#                   'post_zipcode',
#                   'country_id',
#                   'date_of_birth',
#                   'tour')
    
    
    def __init__(self, *args, **kwargs):
        super(AddUserProfileForm, self).__init__(*args, **kwargs)
        self.fields['email'] = forms.EmailField(required=True)
        self.fields['first_name'] = forms.CharField(required=True)
        self.fields['last_name'] = forms.CharField(required=True)
        self.fields['password'] = forms.CharField(required=True)
        self.fields['company'] = forms.IntegerField(required=True)
        self.fields['address'] = forms.CharField(required=True)
        self.fields['town_city'] = forms.CharField(required=True)
        self.fields['county_state'] = forms.CharField(required=True)
        self.fields['post_zipcode'] = forms.CharField(required=True)
        self.fields['country_id'] = forms.IntegerField(required=True)
        self.fields['date_of_birth'] = forms.DateField(required=False)
        self.fields['user_group'] = forms.IntegerField(required=False)
        
class AddNewUserForm(ModelForm):
    class Meta:
        model = UserProfile
        exclude= ('user',
                  'user_group',
                  'access_token',
                  'liveflyte_registered',
                  'registration_token',
                  'password_token',
                  'user_id',
                  'tour')
#         exclude= ('user',
#                   'user_group',
#                   'access_token',
#                   'qtq_registered',
#                   'registration_token',
#                   'password_token',
#                   'customer_id',
#                   'address1',
#                   'town_city', 
#                   'county_state',
#                   'post_zipcode',
#                   'country_id',
#                   'date_of_birth',
#                   'tour')
    
    
    def __init__(self, *args, **kwargs):
        super(AddNewUserForm, self).__init__(*args, **kwargs)
        self.fields['email'] = forms.EmailField(required=True)
        self.fields['first_name'] = forms.CharField(required=True)
        self.fields['last_name'] = forms.CharField(required=True)
        self.fields['password'] = forms.CharField(required=True)
#         self.fields['venue_id'] = forms.CharField(required=False)
        self.fields['address'] = forms.CharField(required=False)
        self.fields['town_city'] = forms.CharField(required=False)
        self.fields['county_state'] = forms.CharField(required=False)
        self.fields['post_zipcode'] = forms.CharField(required=False)
        self.fields['country_id'] = forms.IntegerField(required=True)
        self.fields['date_of_birth'] = forms.DateField(required=False)
        self.fields['user_group'] = forms.IntegerField(required=False)



class EditUserProfileForm(ModelForm):
    class Meta:
        model = UserProfile
        exclude= ('user',
                  'user_group',
                  'access_token',
                  'qtq_registered',
                  'registration_token',
                  'password_token',
                  'user_id',
                  'date_of_birth',
                  'tour')
    
    def __init__(self, *args, **kwargs):
        super(EditUserProfileForm, self).__init__(*args, **kwargs)
        self.fields['password'] = forms.CharField(required=False)
        self.fields['first_name'] = forms.CharField(required=False)
        self.fields['last_name'] = forms.CharField(required=False)
        self.fields['email'] = forms.EmailField(required=False)
        self.fields['password'] = forms.CharField(required=False)
#         self.fields['venue_id'] = forms.CharField(required=False)
        self.fields['company'] = forms.IntegerField(required=True)
        self.fields['address1'] = forms.CharField(required=False)
        self.fields['town_city'] = forms.CharField(required=False)
        self.fields['county_state'] = forms.CharField(required=False)
        self.fields['post_zipcode'] = forms.CharField(required=False)
        self.fields['country_id'] = forms.IntegerField(required=False)
        self.fields['date_of_birth'] = forms.CharField(required=False)
        self.fields['user_group'] = forms.IntegerField(required=False)

class UserProfileForm(ModelForm): 
    class Meta:
        model = UserProfile
        exclude= ('user')
        
    def __init__(self, *args, **kwargs):
        super(UserProfileForm, self).__init__(*args, **kwargs)

class LoginForm(Form): 
        
    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields['username'] = forms.CharField()
        self.fields['password'] = forms.CharField()
        
class AddCompanyForm(ModelForm):
    class Meta:
        model = Company
        exclude= ('state_indicator',
                  'country_id','contact')
                
    def __init__(self, *args, **kwargs):
        super(AddCompanyForm, self).__init__(*args, **kwargs)
        
        self.fields['company_name'] = forms.CharField(required=True)
        self.fields['address1'] = forms.CharField(required=False)
        self.fields['town_city'] = forms.CharField(required=False)
        self.fields['county_state'] = forms.CharField(required=False)
        self.fields['post_zipcode'] = forms.CharField(required=False)
        self.fields['country_id'] = forms.IntegerField(required=False)
        self.fields['phone'] = forms.CharField(required=False)
        self.fields['email'] = forms.EmailField(required=False)
        self.fields['website'] = forms.CharField(required=False)
        self.fields['contact_first_name'] = forms.CharField(required=False)
        self.fields['contact_last_name'] = forms.CharField(required=False)
        self.fields['contact_phone'] = forms.CharField(required=False)
        self.fields['contact_email'] = forms.EmailField(required=True)
        self.fields['contact_title'] = forms.CharField(required=False)
        self.fields['company_type'] = forms.IntegerField(required=False)
#         self.fields['paypal_acc'] = forms.CharField(required=False)
#         self.fields['latitude'] = forms.FloatField(required=False)
#         self.fields['longitude'] = forms.FloatField(required=False)
        self.fields['state_indicator'] = forms.IntegerField(required=False)

class EditCompanyForm(ModelForm):
    class Meta:
        model = Company
        exclude= ('state_indicator',
                  'country_id')
                
    def __init__(self, *args, **kwargs):
        super(EditCompanyForm, self).__init__(*args, **kwargs)
        
        self.fields['company_name'] = forms.CharField(required=True)
        self.fields['address1'] = forms.CharField(required=False)
        self.fields['town_city'] = forms.CharField(required=False)
        self.fields['county_state'] = forms.CharField(required=False)
        self.fields['post_zipcode'] = forms.CharField(required=False)
        self.fields['country_id'] = forms.IntegerField(required=False)
        self.fields['phone'] = forms.CharField(required=False)
        self.fields['email'] = forms.EmailField()
        self.fields['website'] = forms.CharField(required=False)
        self.fields['contact_first_name'] = forms.CharField(required=False)
        self.fields['contact_last_name'] = forms.CharField(required=False)
        self.fields['contact_phone'] = forms.CharField(required=False)
        self.fields['contact_email'] = forms.EmailField()
        self.fields['contact_title'] = forms.CharField(required=False)
        self.fields['company_type'] = forms.IntegerField(required=False)
#         self.fields['paypal_acc'] = forms.CharField(required=False)
#         self.fields['latitude'] = forms.FloatField(required=False)
#         self.fields['longitude'] = forms.FloatField(required=False)
        self.fields['state_indicator'] = forms.IntegerField(required=False)

class AddAirCraftForm(ModelForm):
    class Meta:
        model = AirCraft
        exclude= ('state_indicator',
                  'insert_point','update_point')
                
    def __init__(self, *args, **kwargs):
        super(AddAirCraftForm, self).__init__(*args, **kwargs)
        
        self.fields['aircraft_name'] = forms.CharField(required=True)
        self.fields['company'] = forms.IntegerField(required=True)
        self.fields['serial_no'] = forms.CharField(required=True)
        self.fields['uas_name'] = forms.CharField(required=True)
        self.fields['registration_no'] = forms.CharField(required=False)
        self.fields['wingspan'] = forms.DecimalField(required=False)
        self.fields['length'] = forms.DecimalField(required=False)
        self.fields['mtom'] = forms.DecimalField(required=False)
        self.fields['no_engines'] = forms.IntegerField(required=False)
        self.fields['engine_capacity'] = forms.IntegerField(required=False)
        self.fields['ctrl_frequency'] = forms.DecimalField(required=False)
        self.fields['registered_date'] = forms.DateTimeField(required=False)

class EditAirCraftForm(ModelForm):
    class Meta:
        model = AirCraft
        exclude= ('state_indicator',
                  'insert_point','update_point')
                
    def __init__(self, *args, **kwargs):
        super(EditAirCraftForm, self).__init__(*args, **kwargs)
        
        self.fields['aircraft_name'] = forms.CharField(required=True)
        self.fields['company'] = forms.IntegerField(required=True)
        self.fields['serial_no'] = forms.CharField(required=True)
        self.fields['uas_name'] = forms.CharField(required=True)
        self.fields['registration_no'] = forms.CharField(required=False)
        self.fields['wingspan'] = forms.DecimalField(required=False)
        self.fields['length'] = forms.DecimalField(required=False)
        self.fields['mtom'] = forms.DecimalField(required=False)
        self.fields['no_engines'] = forms.IntegerField(required=False)
        self.fields['engine_capacity'] = forms.IntegerField(required=False)
        self.fields['ctrl_frequency'] = forms.DecimalField(required=False)
        self.fields['registered_date'] = forms.DateTimeField(required=False)

class AddBatteryForm(ModelForm):
    class Meta:
        model = Battery
        exclude= ('state_indicator',
                  'insert_point','update_point')
                
    def __init__(self, *args, **kwargs):
        super(AddBatteryForm, self).__init__(*args, **kwargs)
        
        self.fields['battery_id'] = forms.CharField(required=True)
        self.fields['company'] = forms.IntegerField(required=True)
        self.fields['serial_no'] = forms.CharField(required=True)
        self.fields['description'] = forms.CharField(required=True)
        self.fields['capacity'] = forms.IntegerField(required=False)
        self.fields['cells'] = forms.IntegerField(required=False)
        self.fields['registered_date'] = forms.DateTimeField(required=False)

class EditBatteryForm(ModelForm):
    class Meta:
        model = Battery
        exclude= ('state_indicator',
                  'insert_point','update_point')
                
    def __init__(self, *args, **kwargs):
        super(EditBatteryForm, self).__init__(*args, **kwargs)
        
        self.fields['battery_id'] = forms.CharField(required=True)
        self.fields['company'] = forms.IntegerField(required=True)
        self.fields['serial_no'] = forms.CharField(required=True)
        self.fields['description'] = forms.CharField(required=True)
        self.fields['capacity'] = forms.IntegerField(required=False)
        self.fields['cells'] = forms.IntegerField(required=False)
        self.fields['registered_date'] = forms.DateTimeField(required=False)

class AddJobForm(ModelForm):
    class Meta:
        model = Job
        exclude= ('state_indicator',
                  'insert_point','update_point','preflight_survey','onsite_survey')
                
    def __init__(self, *args, **kwargs):
        super(AddJobForm, self).__init__(*args, **kwargs)
        
        self.fields['job_no'] = forms.CharField(required=True)
        self.fields['description'] = forms.CharField(required=True)
        self.fields['customer'] = forms.IntegerField(required=True)
        self.fields['site'] = forms.IntegerField(required=True)
        self.fields['job_date'] = forms.DateTimeField(required=False)

class EditJobForm(ModelForm):
    class Meta:
        model = Job
        exclude= ('state_indicator',
                  'insert_point','update_point','preflight_survey','onsite_survey')
                
    def __init__(self, *args, **kwargs):
        super(EditJobForm, self).__init__(*args, **kwargs)
        
        self.fields['job_no'] = forms.CharField(required=True)
        self.fields['description'] = forms.CharField(required=True)
        self.fields['customer'] = forms.IntegerField(required=True)
        self.fields['site'] = forms.IntegerField(required=True)
        self.fields['job_date'] = forms.DateTimeField(required=False)

class AddLogForm(ModelForm):
    class Meta:
        model = Log
        exclude= ('state_indicator',
                  'insert_point','update_point')
                
    def __init__(self, *args, **kwargs):
        super(AddLogForm, self).__init__(*args, **kwargs)
        
        self.fields['flight'] = forms.ForeignKey('Flight')
        self.fields['log_no'] = forms.IntegerField(required=True) 
        self.fields['altitude'] = forms.FloatField( required=False)
        self.fields['latitude'] = forms.FloatField( required=False)
        self.fields['longitude'] = forms.FloatField( required=False)

class EditLogForm(ModelForm):
    class Meta:
        model = Log
        exclude= ('state_indicator',
                  'insert_point','update_point')
                
    def __init__(self, *args, **kwargs):
        super(EditLogForm, self).__init__(*args, **kwargs)
        
        self.fields['flight'] = forms.ForeignKey('Flight')
        self.fields['log_no'] = forms.IntegerField(required=True) 
        self.fields['altitude'] = forms.FloatField( required=False)
        self.fields['latitude'] = forms.FloatField( required=False)
        self.fields['longitude'] = forms.FloatField( required=False)

class AddBlackBoxForm(ModelForm):
    class Meta:
        model = BlackBox
        exclude= ('craft','state_indicator',
                  'insert_point','update_point')
                
    def __init__(self, *args, **kwargs):
        super(AddBlackBoxForm, self).__init__(*args, **kwargs)
        
        self.fields['appid'] = forms.CharField(required=True)
        self.fields['craft'] = forms.ForeignKey('AirCraft') 

class EditlackBoxForm(ModelForm):
    class Meta:
        model = BlackBox
        exclude= ('state_indicator',
                  'insert_point','update_point')
                
    def __init__(self, *args, **kwargs):
        super(EditlackBoxForm, self).__init__(*args, **kwargs)
        
        self.fields['appid'] = forms.CharField(required=True)
        self.fields['craft'] = forms.ForeignKey('AirCraft') 


class AddSiteForm(ModelForm):
    class Meta:
        model = Site
        exclude= ('state_indicator',
                  'insert_point','update_point')
                
    def __init__(self, *args, **kwargs):
        super(AddSiteForm, self).__init__(*args, **kwargs)
        
        self.fields['name'] = forms.CharField(required=True)
        self.fields['description'] = forms.CharField(required=True)
        self.fields['latitude'] = forms.FloatField(required=True)
        self.fields['longitude'] = forms.FloatField(required=True)
        self.fields['altitude'] = forms.FloatField(required=False)
        self.fields['address1'] = forms.CharField(required=False)
        self.fields['town_city'] = forms.CharField(required=False)
        self.fields['county_state'] = forms.CharField(required=False)
        self.fields['post_zipcode'] = forms.CharField(required=False)
        self.fields['country_id'] = forms.IntegerField(required=False)

class EditSiteForm(ModelForm):
    class Meta:
        model = Site
        exclude= ('state_indicator',
                  'insert_point','update_point')
                
    def __init__(self, *args, **kwargs):
        super(EditSiteForm, self).__init__(*args, **kwargs)
        
        self.fields['name'] = forms.CharField(required=True)
        self.fields['description'] = forms.CharField(required=True)
        self.fields['latitude'] = forms.FloatField(required=True)
        self.fields['longitude'] = forms.FloatField(required=True)
        self.fields['altitude'] = forms.FloatField(required=False)
        self.fields['address1'] = forms.CharField(required=False)
        self.fields['town_city'] = forms.CharField(required=False)
        self.fields['county_state'] = forms.CharField(required=False)
        self.fields['post_zipcode'] = forms.CharField(required=False)
        self.fields['country_id'] = forms.IntegerField(required=False)

class AddFlightForm(ModelForm):
    class Meta:
        model = Flight
        exclude= ('start_flight','end_flight','state_indicator',
                  'insert_point','update_point')
                
    def __init__(self, *args, **kwargs):
        super(AddFlightForm, self).__init__(*args, **kwargs)
        
        self.fields['flight_no'] = forms.IntegerField(required=True)
        self.fields['description'] = forms.CharField(required=True)
        self.fields['company'] = forms.IntegerField(required=True)
        self.fields['flight_date'] = forms.DateTimeField(required=True)
        self.fields['pic'] = forms.IntegerField(required=False)
        self.fields['observer'] = forms.IntegerField(required=False)
        self.fields['payload_operator'] = forms.IntegerField(required=False)
        self.fields['spotter'] = forms.IntegerField(required=False)
        self.fields['latitude'] = forms.FloatField(required=False)
        self.fields['longitude'] = forms.FloatField(required=False)

class EditFlightForm(ModelForm):
    class Meta:
        model = Flight
        exclude= ('start_flight','end_flight','state_indicator',
                  'insert_point','update_point')
                
    def __init__(self, *args, **kwargs):
        super(EditFlightForm, self).__init__(*args, **kwargs)
        
        self.fields['flight_no'] = forms.IntegerField(required=True)
        self.fields['description'] = forms.CharField(required=True)
        self.fields['company'] = forms.IntegerField(required=True)
        self.fields['flight_date'] = forms.DateTimeField(required=True)
        self.fields['pic'] = forms.IntegerField(required=False)
        self.fields['observer'] = forms.IntegerField(required=False)
        self.fields['payload_operator'] = forms.IntegerField(required=False)
        self.fields['spotter'] = forms.IntegerField(required=False)
        self.fields['latitude'] = forms.FloatField(required=False)
        self.fields['longitude'] = forms.FloatField(required=False)

class AddPreFlightSurveyForm(ModelForm):
    class Meta:
        model = PreFlightSurvey
        exclude= ('state_indicator',
                  'insert_point','update_point')
                
    def __init__(self, *args, **kwargs):
        super(AddPreFlightSurveyForm, self).__init__(*args, **kwargs)
        
        self.fields['survey_date'] = forms.DateTimeField(required=True)
        self.fields['airspace'] = forms.CharField(required=True)
        self.fields['terrain'] = forms.CharField(required=True)
        self.fields['proximities'] = forms.CharField(required=True)
        self.fields['hazards'] = forms.CharField(required=False)
        self.fields['restrictions'] = forms.CharField(required=False)
        self.fields['sensitivities'] = forms.CharField(required=False)
        self.fields['people'] = forms.CharField(required=False)
        self.fields['livestock'] = forms.CharField(required=False)
        self.fields['permission'] = forms.CharField(required=False)
        self.fields['access'] = forms.CharField(required=True)
        self.fields['cordon'] = forms.CharField(required=True)
        self.fields['footpaths'] = forms.CharField(required=True)
        self.fields['alternate'] = forms.CharField(required=True)
        self.fields['risk_mitigation'] = forms.CharField(required=False)
        self.fields['weather'] = forms.CharField(required=False)
        self.fields['notams'] = forms.CharField(required=False)
        self.fields['local_atc_contact'] = forms.CharField(required=False)
        self.fields['local_atc_contact_date'] = forms.DateTimeField(required=False)
        self.fields['regional_atc_contact'] = forms.CharField(required=False)
        self.fields['regional_atc_contact_date'] = forms.DateTimeField(required=False)
        self.fields['military_control_conact'] = forms.CharField(required=False)
        self.fields['military_control_conact_date'] = forms.DateTimeField(required=False)
        self.fields['notice_to_airmen'] = forms.CharField(required=False)

class EditPreFlightSurveyForm(ModelForm):
    class Meta:
        model = PreFlightSurvey
        exclude= ('state_indicator',
                  'insert_point','update_point')
                
    def __init__(self, *args, **kwargs):
        super(EditPreFlightSurveyForm, self).__init__(*args, **kwargs)
        
        self.fields['survey_date'] = forms.DateTimeField(required=True)
        self.fields['airspace'] = forms.CharField(required=True)
        self.fields['terrain'] = forms.CharField(required=True)
        self.fields['proximities'] = forms.CharField(required=True)
        self.fields['hazards'] = forms.CharField(required=False)
        self.fields['restrictions'] = forms.CharField(required=False)
        self.fields['sensitivities'] = forms.CharField(required=False)
        self.fields['people'] = forms.CharField(required=False)
        self.fields['livestock'] = forms.CharField(required=False)
        self.fields['permission'] = forms.CharField(required=False)
        self.fields['access'] = forms.CharField(required=True)
        self.fields['cordon'] = forms.CharField(required=True)
        self.fields['footpaths'] = forms.CharField(required=True)
        self.fields['alternate'] = forms.CharField(required=True)
        self.fields['risk_mitigation'] = forms.CharField(required=False)
        self.fields['weather'] = forms.CharField(required=False)
        self.fields['notams'] = forms.CharField(required=False)
        self.fields['local_atc_contact'] = forms.CharField(required=False)
        self.fields['local_atc_contact_date'] = forms.DateTimeField(required=False)
        self.fields['regional_atc_contact'] = forms.CharField(required=False)
        self.fields['regional_atc_contact_date'] = forms.DateTimeField(required=False)
        self.fields['military_control_conact'] = forms.CharField(required=False)
        self.fields['military_control_conact_date'] = forms.DateTimeField(required=False)
        self.fields['notice_to_airmen'] = forms.CharField(required=False)

class AddOnSiteSurveyForm(ModelForm):
    class Meta:
        model = OnSiteSurvey
        exclude= ('state_indicator',
                  'insert_point','update_point')
                
    def __init__(self, *args, **kwargs):
        super(AddOnSiteSurveyForm, self).__init__(*args, **kwargs)
        
        self.fields['survey_date'] = forms.DateTimeField(required=True)
        self.fields['wind_speed'] = forms.FloatField(required=True)
        self.fields['wind_direction'] = forms.CharField(required=True)
        self.fields['temperature'] = forms.FloatField(required=True)
        self.fields['obstructions'] = forms.CharField(required=False)
        self.fields['visual_limitations'] = forms.CharField(required=False)
        self.fields['cordon'] = forms.CharField(required=False)
        self.fields['livestock'] = forms.CharField(required=False)
        self.fields['terrain'] = forms.CharField(required=False)
        self.fields['permission'] = forms.CharField(required=False)
        self.fields['public'] = forms.CharField(required=True)
        self.fields['air_traffic'] = forms.CharField(required=True)
        self.fields['communication'] = forms.CharField(required=True)
        self.fields['proximity'] = forms.CharField(required=True)
        self.fields['take_off_area'] = forms.CharField(required=False)
        self.fields['landing_area'] = forms.CharField(required=False)
        self.fields['operational_zone'] = forms.CharField(required=False)
        self.fields['emergency_area'] = forms.CharField(required=False)
        self.fields['local_police_telephone'] = forms.CharField(required=False)
        self.fields['local_hospital_telephone'] = forms.CharField(required=False)
        self.fields['local_atc_telephone'] = forms.CharField(required=False)
        self.fields['notes'] = forms.CharField(required=False)

class EditOnSiteSurveyForm(ModelForm):
    class Meta:
        model = OnSiteSurvey
        exclude= ('state_indicator',
                  'insert_point','update_point')
                
    def __init__(self, *args, **kwargs):
        super(EditOnSiteSurveyForm, self).__init__(*args, **kwargs)
        
        self.fields['survey_date'] = forms.DateTimeField(required=True)
        self.fields['wind_speed'] = forms.FloatField(required=True)
        self.fields['wind_direction'] = forms.CharField(required=True)
        self.fields['temperature'] = forms.FloatField(required=True)
        self.fields['obstructions'] = forms.CharField(required=False)
        self.fields['visual_limitations'] = forms.CharField(required=False)
        self.fields['cordon'] = forms.CharField(required=False)
        self.fields['livestock'] = forms.CharField(required=False)
        self.fields['terrain'] = forms.CharField(required=False)
        self.fields['permission'] = forms.CharField(required=False)
        self.fields['public'] = forms.CharField(required=True)
        self.fields['air_traffic'] = forms.CharField(required=True)
        self.fields['communication'] = forms.CharField(required=True)
        self.fields['proximity'] = forms.CharField(required=True)
        self.fields['take_off_area'] = forms.CharField(required=False)
        self.fields['landing_area'] = forms.CharField(required=False)
        self.fields['operational_zone'] = forms.CharField(required=False)
        self.fields['emergency_area'] = forms.CharField(required=False)
        self.fields['local_police_telephone'] = forms.CharField(required=False)
        self.fields['local_hospital_telephone'] = forms.CharField(required=False)
        self.fields['local_atc_telephone'] = forms.CharField(required=False)
        self.fields['notes'] = forms.CharField(required=False)


class AddPersonForm(ModelForm):
    class Meta:
        model = Person
        exclude= ('state_indicator',
                  'insert_point','update_point')
    
    def __init__(self, *args, **kwargs):
        super(AddPersonForm, self).__init__(*args, **kwargs)
        self.fields['first_name'] = forms.CharField(required=True)
        self.fields['last_name'] = forms.CharField(required=True)
        self.fields['email'] = forms.EmailField(required=True)
        self.fields['address'] = forms.CharField(required=True)
        self.fields['town_city'] = forms.CharField(required=True)
        self.fields['county_state'] = forms.CharField(required=True)
        self.fields['post_zipcode'] = forms.CharField(required=True)
        self.fields['country_id'] = forms.IntegerField(required=True)
        


class EditPersonForm(ModelForm):
    class Meta:
        model = Person
        exclude= ('state_indicator',
                  'insert_point','update_point')
    
    def __init__(self, *args, **kwargs):
        super(EditPersonForm, self).__init__(*args, **kwargs)
        self.fields['first_name'] = forms.CharField(required=True)
        self.fields['last_name'] = forms.CharField(required=True)
        self.fields['email'] = forms.EmailField(required=True)
        self.fields['address'] = forms.CharField(required=True)
        self.fields['town_city'] = forms.CharField(required=True)
        self.fields['county_state'] = forms.CharField(required=True)
        self.fields['post_zipcode'] = forms.CharField(required=True)
        self.fields['country_id'] = forms.IntegerField(required=True)

class PersonForm(ModelForm): 
    class Meta:
        model = Person
        
    def __init__(self, *args, **kwargs):
        super(PersonForm, self).__init__(*args, **kwargs)



class AddStaffForm(ModelForm):
    class Meta:
        model = Staff
        exclude= ('state_indicator',
                  'insert_point','update_point')
                
    def __init__(self, *args, **kwargs):
        super(AddStaffForm, self).__init__(*args, **kwargs)
        
        self.fields['name'] = forms.CharField(required=True)
        self.fields['company'] = forms.IntegerField(required=True)
        self.fields['role'] = forms.IntegerField(required=True)

class EditStaffForm(ModelForm):
    class Meta:
        model = Staff
        exclude= ('state_indicator',
                  'insert_point','update_point')
                
    def __init__(self, *args, **kwargs):
        super(EditStaffForm, self).__init__(*args, **kwargs)
        
        self.fields['name'] = forms.CharField(required=True)
        self.fields['company'] = forms.IntegerField(required=True)
        self.fields['role'] = forms.IntegerField(required=True)

class AddRoleForm(ModelForm):
    class Meta:
        model = Role
        exclude= ('state_indicator',
                  'insert_point','update_point')
                
    def __init__(self, *args, **kwargs):
        super(AddRoleForm, self).__init__(*args, **kwargs)
        
        self.fields['title'] = forms.CharField(required=True)

class EditRoleForm(ModelForm):
    class Meta:
        model = Role
        exclude= ('state_indicator',
                  'insert_point','update_point')
                
    def __init__(self, *args, **kwargs):
        super(EditRoleForm, self).__init__(*args, **kwargs)
        
        self.fields['title'] = forms.CharField(required=True)



class AddDeviceForm(ModelForm):
    class Meta:
        model = User_Device
        
        
    def __init__(self, *args, **kwargs):
        super(AddDeviceForm, self).__init__(*args, **kwargs)
        
        self.fields['user'] = forms.CharField(required=True)
        self.fields['model'] = forms.CharField(required=False)
        self.fields['notification_token'] = forms.CharField(required=False)
        self.fields['platform'] = forms.CharField(required=False)
        self.fields['uuid'] = forms.CharField(required=False)
        self.fields['version'] = forms.CharField(required=False)        
    
  
class AddUserGroupForm(ModelForm):
	class Meta:
		model = UserGroup
		
	def __init__(self, *args, **kwags):
		super(AddUserGroupForm, self).__init__(*args, **kwags)
		
		self.fields['name'] = forms.CharField(required=True)  


class AddCountryForm(ModelForm):
    class Meta:
        model = Country
                
    def __init__(self, *args, **kwargs):
        super(AddCountryForm, self).__init__(*args, **kwargs)
        
        self.fields['name'] = forms.CharField(required=True)
        self.fields['iso_2'] = forms.CharField(required=True)
        self.fields['iso_3'] = forms.CharField(required=True)
        self.fields['dial_prefix'] = forms.CharField(required=True)
        self.fields['currency_code'] = forms.IntegerField(required=True)
        self.fields['currency_symbol'] = forms.IntegerField(required=True)