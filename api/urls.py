from django.conf.urls.defaults import *
from api.views import GetFlightsView, GetFlightsShortView, GetLogsView, GetPilotsView, GetCraftView, GetPeopleView, GetBatteriesView, GetFlightBatteriesView, GetIncidentsView, GetBlackBoxesView
from api.views import GetCompanyView, GetCompaniesView
from api.views import UserDetailView, UserLoginView, UserActivateView, AddNewUserView, EditUserView, ResetPassword, ValidatePassword, ChangePassword, ResendActivation
from api.views import UserLogoutView, UsersView, CompanyUsersView, UserView, CompanyView, ThisCompanyView, AddNewCompanyView, CompanyDetailView, EditCompanyView, CompanyDeleteView, CompanyActivateView, CompanyDeactivateView
from api.views import UserDeleteView, UserSetActiveView, UserSetDeactiveView, UserGroupsView, CountriesView
from api.views import StaffMembersView, EditStaffMemberView, StaffView, StaffDeleteView, AddStaffMember, StaffSetDeactiveView, StaffSetActiveView
from api.views import ResetUserView, AppResetPassword
from api.views import GetNewConfigView, LogonView, RegisterBatteryView, LogFlightsView, CloseFlightLogView
from api.views import RegisterNotificationDevice

from django.conf.urls.defaults import handler404
 
from api.views import UserRegisterView

from djangorestframework.views import ListOrCreateModelView 

from djangorestframework.resources import ModelResource
from api.models import UserProfile
from models import User
#from models import User, Book
handler404 = 'api.views.my_custom_404_view'

    
class UserProfileResource(ModelResource):
    model = UserProfile
    
class UserResource(ModelResource):
    model = User

urlpatterns = patterns('',
                       
                       url(r'^getNewConfig/$', GetNewConfigView.as_view(), name='getNewConfigView'), #---
                       url(r'^logon/(?P<appid>\w+)/$', LogonView.as_view(), name='logonView'), #---
                       url(r'^battery/(?P<appid>\w+)/(?P<flightid>\d+)/(?P<battid>\w+)/$', RegisterBatteryView.as_view(), name='registerBatteryView'), #---
                       url(r'^log/(?P<appid>\w+)/(?P<lat>.*)/(?P<lon>.*)/(?P<alti>.*)/$', LogFlightsView.as_view(), name='logFlightsView'), #---
#                        url(r'^log/(?P<appid>\w+)/(?P<lat>\d+)/(?P<lon>\d+)/(?P<alti>\d+)/(?P<timestamp>\d+)/$', LogFlightsView.as_view(), name='logFlightsView'), #---
                       url(r'^bye/(?P<appid>\w+)/$', CloseFlightLogView.as_view(), name='closeFlightLogView'), #---
                       
                       url(r'^flights/$', GetFlightsView.as_view(), name='GetFlightsView'), #---
                       url(r'^flightList/$', GetFlightsShortView.as_view(), name='GetFlightsShortView'), #---
                       url(r'^logs/(?P<craftid>\w+)/(?P<flightid>\d+)/$', GetLogsView.as_view(), name='GetFlightsView'), #---
                       url(r'^company/(?P<companyid>\w+)/$', GetCompanyView.as_view(), name='GetCompanyView'), #---
                       url(r'^companies/$', GetCompaniesView.as_view(), name='GetCompaniesView'), #---
                       url(r'^pilots/$', GetPilotsView.as_view(), name='GetPilotsView'), #---
                       url(r'^craft/$', GetCraftView.as_view(), name='GetCraftView'), #---
                       url(r'^people/$', GetPeopleView.as_view(), name='GetPeopleView'), #---
                       url(r'^batteries/$', GetBatteriesView.as_view(), name='GetBatteriesView'), #---
                       url(r'^blackboxes/$', GetBlackBoxesView.as_view(), name='GetBlackBoxesView'), #---
                       url(r'^flightBatteries/(?P<craftid>\w+)/(?P<flightid>\d+)/$', GetFlightBatteriesView.as_view(), name='GetBatteriesView'), #---
                       url(r'^incidents/$', GetIncidentsView.as_view(), name='GetIncidentsView'), #---
                       
                       url(r'^mycompany/view/$', ThisCompanyView.as_view(), name='getMyCompanyView'),
                       url(r'^company/view/$', CompanyView.as_view(), name='getCompanyView'),
                       url(r'^company/delete/(?P<company_id>\d+)/$', CompanyDeleteView.as_view(), name='getCompanyDelete'),
                       url(r'^company/deactivate/(?P<company_id>\d+)/$', CompanyDeactivateView.as_view(), name='getCompanyDeactivate'),
                       url(r'^company/activate/(?P<company_id>\d+)/$', CompanyActivateView.as_view(), name='getCompanyActivate'),
                       url(r'^company/users/(?P<company_id>\d+)/$', CompanyUsersView.as_view(), name='getCompanyUsers'),
                       url(r'^company/(?P<company_id>\d+)/$', CompanyDetailView.as_view(), name='getCompanyDetail'),
                       url(r'^company/edit/(?P<company_id>\d+)/$', EditCompanyView.as_view(), name='editCompany'),
                       url(r'^company/add/$', AddNewCompanyView.as_view(), name='addCompany'),
                       
                       url(r'^staff/members/(?P<company_id>\d+)/$', StaffMembersView.as_view(), name='getStaffMembers'),
                       url(r'^staff/edit/(?P<staff_id>\d+)/$', EditStaffMemberView.as_view(), name='editStaffMember'),
                       url(r'^staff/view/(?P<staff_id>\d+)/$', StaffView.as_view(), name='viewStaffMember'),
                       url(r'^staff/delete/(?P<staff_id>\d+)/$', StaffDeleteView.as_view(), name='deleteStaffMember'),
                       url(r'^staff/activate/(?P<staff_id>\d+)/$', StaffSetActiveView.as_view(), name='activateStaffMember'),
                       url(r'^staff/deactivate/(?P<staff_id>\d+)/$', StaffSetDeactiveView.as_view(), name='deactivateStaffMember'),
                       url(r'^staff/add/(?P<company_id>\d+)/$', AddStaffMember.as_view(), name='addStaffMember'),
                       
                       url(r'^user/register/$', UserRegisterView.as_view(), name='addUserProfileT'), #---
                       url(r'^user/addnew/$', AddNewUserView.as_view(), name='addAddNewUserT'), #---
                       url(r'^user/activate/(?P<user_id>\d+)/(?P<hash>\w+)/$', UserActivateView.as_view(), name='activateUserProfileT'), #---
                       url(r'^user/login/$', UserLoginView.as_view(), name='qtq-login'), #---
                       url(r'^user/logout/$', UserLogoutView.as_view(), name='logout'), #---
                       url(r'^user/$', UserDetailView.as_view(), name='editUserProfileT'),
                       url(r'^user/info/$', UserDetailView.as_view(), name='editUserProfileT'),
                       url(r'^users/(?P<group_id>\d+)/$', UsersView.as_view(), name='getUsersT'), 
                       
                       url(r'^user/password/reset/$',ResetPassword.as_view(), name='resetPassword'),
                       url(r'^user/password/validate/(?P<hash>\w+)/$',ValidatePassword.as_view(), name='validatePasswordT'),
                       url(r'^user/password/change/$',ChangePassword.as_view(), name='changePasswordT'),
                       url(r'^user/activate/resend/$',ResendActivation.as_view(), name='resendActivation'),
                       url(r'^user/register/device/$',RegisterNotificationDevice.as_view(), name='resendActivation'),
                       url(r'^getuser/(?P<user_id>\d+)/$', UserView.as_view(), name='getUserT'),
                       url(r'^reset/getuser/$', ResetUserView.as_view(), name='getUserT'),
                       url(r'^reset/password/$', AppResetPassword.as_view(), name='getUserT'),
                       url(r'^user/edit/(?P<user_id>\d+)/$', EditUserView.as_view(), name='editUserT'),
                       url(r'^user/delete/(?P<user_id>\d+)/$', UserDeleteView.as_view(), name='deleteUserT'),
                       url(r'^user/setactive/(?P<user_id>\d+)/$', UserSetActiveView.as_view(), name='activateUserT'),
                       url(r'^user/setdeactivate/(?P<user_id>\d+)/$', UserSetDeactiveView.as_view(), name='deactivateUserT'),
                       
                       url(r'^data/user_groups/$',UserGroupsView.as_view(), name='resendActivation'),
                       url(r'^data/countries/$',CountriesView.as_view(), name='resendActivation'),
                       
                       )



