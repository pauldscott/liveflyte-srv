from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm, Form
from models import UserProfile, Book, Box, Entry, Event, Photo, Note, Collaborators

class DisableCSRF(object):

    def process_request(self, request):

        setattr(request, '_dont_enforce_csrf_checks', True)
    

class AddUserProfileForm(ModelForm):
    class Meta:
        model = UserProfile
        exclude= ('user',
                  'friends',
                  'boxes',
                  'books',
                  'events',
                  'facebook_username',
                  'facebook_email',
                  'facebook_id',
                  'notifications',
                  'access_token',
                  'facebook_registered',
                  'treasured_registered',
                  'registration_token', 
                  'space_used',
                  'usage_plan',
                  'username')
    
    
    def __init__(self, *args, **kwargs):
        super(AddUserProfileForm, self).__init__(*args, **kwargs)
        self.fields['email'] = forms.EmailField(required=True)
        self.fields['first_name'] = forms.CharField(required=True)
        self.fields['last_name'] = forms.CharField(required=True)
        self.fields['password'] = forms.CharField(required=True)
        
        


class EditUserProfileForm(ModelForm):
    class Meta:
        model = UserProfile
        exclude= ('facebook_id', 
                  'facebook_email', 
                  'facebook_username', 
                  'user', 
                  'events', 
                  'boxes', 
                  'books',
                  'notifications', 
                  'friends', 
                  'access_token',
                  'facebook_registered',
                  'treasured_registered',
                  'registration_token',
                  'space_used',
                  'usage_plan'
                  )
    
    def __init__(self, *args, **kwargs):
        super(EditUserProfileForm, self).__init__(*args, **kwargs)
        self.fields['password'] = forms.CharField(required=False)
        self.fields['first_name'] = forms.CharField()
        self.fields['last_name'] = forms.CharField()
        self.fields['email'] = forms.EmailField()

class UserProfileForm(ModelForm): 
    class Meta:
        model = UserProfile
        exclude= ('user')
        
    def __init__(self, *args, **kwargs):
        super(UserProfileForm, self).__init__(*args, **kwargs)

class LoginForm(Form): 
        
    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields['username'] = forms.CharField()
        self.fields['password'] = forms.CharField()
        
#        self.fields['first_name'] = forms.CharField()
#        self.fields['last_name'] = forms.CharField()
#        self.fields['email'] = forms.EmailField(required=False)
 
class BoxForm(ModelForm):
    
    class Meta:
        model = Box
    
    def __init__ (self, *args, **kwargs):
        super(BoxForm, self).__init__(*args, **kwargs)
        self.fields['entries'] = forms.MultipleChoiceField(required = False)
        self.fields['cover_photo'] = forms.FileField(required = False)
        self.fields['date_of_birth'] = forms.CharField()
        self.fields['box_type2'] = forms.CharField(required = False)
        
class BookForm(ModelForm):
    
    class Meta:
        model = Book
    
    def __init__ (self, *args, **kwargs):
        super(BookForm, self).__init__(*args, **kwargs)
        self.fields['entries'] = forms.MultipleChoiceField(required = False)
        self.fields['front_cover_photo'] = forms.MultipleChoiceField(queryset=Photo.objects.all(),required = False)
        self.fields['back_cover_photo'] = forms.MultipleChoiceField(queryset=Photo.objects.all(),required = False)
        #self.fields['front_cover_photo'] = forms.FileField(required = False)
        #self.fields['back_cover_photo'] = forms.FileField(required = False)
        self.fields['book_title'] = forms.CharField(required = False)
        self.fields['filename'] = forms.CharField(required = False)
              
class facebookLoginForm(Form):
    
        facebook_token = forms.CharField()
            
            
class AssocBoxEntryForm(Form):
    pass

class AssocBookEntryForm(Form):
    pass
    
class EntryForm(ModelForm):
    
    class Meta:
        model = Entry
        
    def __init__(self, *args, **kwargs):
        super(EntryForm, self).__init__(*args, **kwargs)
        
class EventForm(ModelForm):
    class Meta:
        model = Event
        exclude = ('type')
        
    def __init__(self, *args, **kwargs):
        super(EventForm, self).__init__(*args, **kwargs)
        self.fields['latitude'] = forms.CharField(required = False)
        self.fields['longitude'] = forms.CharField(required = False)
        self.fields['cover_photo'] = forms.FileField(required = False)
        
class DeleteEventForm(Form):
    pass             
        
class AssocEventEntryForm(Form):

    event_id = forms.CharField()
    entry_id = forms.CharField()
    
class PhotoUploadAddForm(ModelForm):
    class Meta:
        model = Entry
        exclude = ('type')
        
    def __init__(self, *args, **kwargs):
        super(PhotoUploadAddForm, self).__init__(*args, **kwargs)
        
        self.fields['photo_caption'] = forms.CharField(required=False)
        self.fields['full_image_size'] = forms.IntegerField(required=False)
        self.fields['photo_file'] = forms.FileField(
            label='Select a file',
            help_text='max. 42 megabytes'
        )
        #print self
                
class PhotoAddForm(ModelForm):
    class Meta:
        model = Entry
        exclude = ('type')
        
    def __init__(self, *args, **kwargs):
        super(PhotoAddForm, self).__init__(*args, **kwargs)
        
        #self.fields['photo'] = forms.CharField()
        self.fields['photo_caption'] = forms.CharField(required=False)
        self.fields['full_image_size'] = forms.IntegerField(required=False)
        self.fields['photo_file'] = forms.FileField(
            label='Select a file',
            help_text='max. 42 megabytes'
        )

class UploadOriginalPhoto(Form):
    photo_file = forms.FileField(
            label='Select a file',
            help_text='max. 42 megabytes'
        )

class PhotoManyBoxesForm(ModelForm):
    class Meta:
        model = Entry
        exclude = ('type')
    
    def __init__(self, *args, **kwargs):
        super(PhotoManyBoxesForm, self).__init__(*args, **kwargs)
        self.fields['photo_caption'] = forms.CharField(required=False)
        self.fields['boxes'] = forms.CharField()
        self.fields['full_image_size'] = forms.IntegerField(required=False)
        self.fields['photo_file'] = forms.FileField(
            label='Select a file',
            help_text='max. 42 megabytes'
        )
        #print self

class PhotoManyEventsForm(ModelForm):
    class Meta:
        model = Entry
        exclude = ('type')
    
    def __init__(self, *args, **kwargs):
        super(PhotoManyEventsForm, self).__init__(*args, **kwargs)
        self.fields['photo_caption'] = forms.CharField(required=False)
        self.fields['events'] = forms.CharField()  
        self.fields['full_image_size'] = forms.IntegerField(required=False)
        self.fields['photo_file'] = forms.FileField(
            label='Select a file',
            help_text='max. 42 megabytes'
        )

        
        
class PhotoEditForm(ModelForm):
    class Meta:
        model = Entry
        exclude = ('type', 'photo_pic', 'latitude', 'longitude')
    
    def __init__(self, *args, **kwargs):
        super(PhotoEditForm, self).__init__(*args, **kwargs)
        self.fields['photo_caption'] = forms.CharField(required=False)        
        
class NoteAddForm(ModelForm):
    class Meta:
        model = Entry
        exclude = ('type')
        
    def __init__(self, *args, **kwargs):
        super(NoteAddForm, self).__init__(*args, **kwargs)
        self.fields['note_text'] = forms.CharField()   
        
        
class NoteManyBoxesForm(ModelForm):
    class Meta:
        model = Entry
        exclude = ('type')
    
    def __init__(self, *args, **kwargs):
        super(NoteManyBoxesForm, self).__init__(*args, **kwargs)
        self.fields['note_text'] = forms.CharField()
        self.fields['boxes'] = forms.CharField()        
    

class NoteManyEventsForm(ModelForm):
    class Meta:
        model = Entry
        exclude = ('type')
    
    def __init__(self, *args, **kwargs):
        super(NoteManyEventsForm, self).__init__(*args, **kwargs)
        self.fields['note_text'] = forms.CharField()
        self.fields['events'] = forms.CharField()        
    
    
class NoteEditForm(ModelForm):
    class Meta:
        model = Entry
        exclude = ('type', 'latitude', 'longitude')
    
    def __init__(self, *args, **kwargs):
        super(NoteEditForm, self).__init__(*args, **kwargs)
        self.fields['note_text'] = forms.CharField()

#class AddCollaboratorBoxForm(Form):  
#              
#    user_id = forms.CharField()
#    #box_id = forms.CharField()    
#    is_collaborator = forms.BooleanField(required = False)
#    is_administrator = forms.BooleanField(required = False)    

class CollaboratorsForm(ModelForm):
    class Meta:
        model = Collaborators
        exclude = ('box')
        
    def __init__(self, *args, **kwargs):
        super(CollaboratorsForm, self).__init__(*args, **kwargs)
        
        self.fields['user'] = forms.CharField(required = False)
        self.fields['can_read'] = forms.BooleanField(required = False)
        self.fields['can_write'] = forms.BooleanField(required = False)
        self.fields['is_owner'] = forms.BooleanField(required = False)
        self.fields['req_approval'] = forms.BooleanField(required = False)
        
        
class AddFriendForm(Form):
    
    friend_email = forms.CharField()
    
    
class ApproveEntryInBox(Form):
    
    box_id = forms.CharField()
    notif_id = forms.CharField()
    approved = forms.BooleanField(required = False)
    
class DeleteEntryFromBox(Form):
    pass

class InviteFriendsWithFacebook(Form):
    friendsArray = forms.CharField()
    
    
