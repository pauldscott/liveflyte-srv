#coding=utf-8 
from PIL import Image
from api.forms import LoginForm, AddUserProfileForm, AddNewUserForm, EditUserProfileForm, AddCompanyForm, EditCompanyForm, AddCountryForm, \
UserProfileForm, AddAirCraftForm, EditAirCraftForm, AddBatteryForm, EditBatteryForm, AddJobForm, \
EditJobForm, AddSiteForm, EditSiteForm, AddFlightForm, EditFlightForm, AddPreFlightSurveyForm, \
EditPreFlightSurveyForm, AddOnSiteSurveyForm, EditOnSiteSurveyForm, AddStaffForm, EditStaffForm, \
AddRoleForm, EditRoleForm, AddDeviceForm, AddUserGroupForm, AddCountryForm
from api.forms import AddLogForm, EditLogForm, AddBlackBoxForm, EditlackBoxForm
from api.models import UserProfile, Company, AirCraft, Battery, CompanyType, Job, Site, Flight, PreFlightSurvey, OnSiteSurvey, Staff, Role,  User_Device, UserGroup, Country, Log, BlackBox, FlightBattery
from datetime import datetime
from datetime import date
from django.utils import timezone
from decimal import Decimal
from django import forms
from django.conf import settings, Settings
from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.core.mail import EmailMessage
from django.core.servers.basehttp import FileWrapper
from django.core.urlresolvers import reverse
from django.core.files.images import get_image_dimensions
from django.db.models import Q, Avg, Max, Count, Sum
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.views.decorators.cache import cache_page
from django.views.decorators.csrf import csrf_exempt
from djangorestframework import status, permissions
from djangorestframework.permissions import IsAuthenticated
from djangorestframework.response import Response
from djangorestframework.views import View
from django.db.models import Max
from ecl_facebook import Facebook
from ecl_facebook.django_decorators import facebook_begin, facebook_callback
from email.MIMEImage import MIMEImage
from operator import attrgetter
from random import choice
from itertools import chain
from collections import namedtuple
from uuidfield import UUIDField
from push_notifications.models import APNSDevice, GCMDevice
import six
import base64
import hashlib
import hmac
import json
import md5
import os
import random
import sha
import urllib2
import urllib
import time
import pytz
import math
import uuid
#import utc


# from braintree import *

from django.views.decorators.cache import cache_page
from django.conf.locale import el
from liveFlyteServer.settings import NOTE_SIZE
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.contrib.sessions.backends.base import SessionBase
# from braintree.subscription import Subscription


@cache_page(15 * 60)
def my_custom_404_view(request):
    print 'redirecting'
    return HttpResponseRedirect("/site/404.html")


# braintree.Configuration.configure(braintree.Environment.Production,
#                                   merchant_id="252m9xxzgfygxcmy",
#                                   public_key="gbp2msq8bxypx46d",
#                                   private_key="d1624644be066c1171d685b6e6002518")

#braintree.Configuration.configure(braintree.Environment.Sandbox,
#                                  merchant_id="nn6fkypynzsqd9ry",
#                                  public_key="g5y78h8kbqsp8sb4",
#                                  private_key="542425ab614a85d292db89c62c2cf3a9")



# USER VIEWS
class AddNewUserView(View):  
    ''' 
    LiveFlyte Users : Adds a User (a Django User will be automatically created)
    '''     
    
    form = AddNewUserForm
    def post(self, request):
        print "ADDING A NEW USER"
        #Field Validation
        email = self.CONTENT['email']
        first_name = self.CONTENT['first_name']
        last_name = self.CONTENT['last_name']
        password = self.CONTENT['password']
        
        print "we are here"

        if validateEmail(email) == False:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '5', 'error_message':'Invalid e-mail address'})
        
        if first_name == None or len(first_name) == 0:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '6', 'error_message':'First Name is not filled'})
        
        if last_name == None or len(last_name) == 0:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '6', 'error_message':'Last Name is not filled'})
        
        if password == None or len(password) == 0:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '7', 'error_message':'Password is not filled'})
            
        
        email = email.lower() 
        user = None
        
        try: 
            print "we are here 2"
            user = User.objects.get(email = email)
            
        except:
            pass
        
        
        if user != None:
            print "We have a User"
            qtq_user = None
            
            try:
                qtq_user = UserProfile.objects.get(user = user)
            except:
                pass
            
            
            if qtq_user != None:
                print "we are here 3"
                if qtq_user.qtq_registered == False:
                        qtq_user.qtq_registered = True
                        qtq_user.save()
                        user.set_password(self.CONTENT['password'])
                        print "we are here 4"
                        user.save()
                        
                        return {
                                    "username":qtq_user.user.username,
                                    "first_name":qtq_user.user.first_name,
                                    "email":qtq_user.user.email,
                                    "active":qtq_user.user.is_active
                                }
                
                elif qtq_user.qtq_registered == True:
                    return Response(status.HTTP_401_UNAUTHORIZED, {'error_code': '1', 'error_message':"Have you already registered with QuitTheQ? We already have an account for this email address. If you can't remember your password, please reset it."})
                
                else:
                    user.first_name = self.CONTENT['first_name']
                    user.last_name = self.CONTENT['last_name']
                    user.is_active = False
                    user.set_password(self.CONTENT['password'])
                    
                    user.save()
                    
#                     qtq_user.treasured_registered = True
#                    qtq_user.save()
#                    user.is_active = False
                    
                    random_number = random.randrange(0, 10000)
                    sha_key = sha.new(str(random_number))
                    sha_digest = sha_key.hexdigest()
                
                    qtq_user = UserProfile.objects.get(user = user)
                    venue_id = self.CONTENT['venue_id']
                    venue_name = ""
                    print "venue id =%s" % venue_id
                    if venue_id != None and venue_id != "": 
                        venue_id = int(venue_id)
                        qtq_user.venue_id = venue_id
                        venue = Venue.objects.get(id = venue_id)
                        if venue != None:
                            venue_name = venue.venue_name
                    
                    qtq_user.venue_name = venue_name
                    qtq_user.qtq_registered = True
                    qtq_user.registration_token = sha_digest
                    qtq_user.address = self.CONTENT['address']
                    
                    qtq_user.town_city = self.CONTENT['town_city']
                    qtq_user.county_state = self.CONTENT['county_state']
                    qtq_user.post_zipcode = self.CONTENT['post_zipcode']
                    qtq_user.country_id = self.CONTENT['country_id']
                    qtq_user.date_of_birth = self.CONTENT['date_of_birth']
                    user_group = UserGroup.objects.get(id = self.CONTENT['user_group'])
                    if user_group == "":
                        user_group = UserGroup.objects.get(name = 'Master Admin')
                    
                    qtq_user.user_group = user_group
                    
                    qtq_user.save()
                    
                    
                    
                    body = '<p>Hi, you have been registered for a QuitTheQ account, the next step is easy.</p> \
<p>To activate your account just click on the link <a href=\'' + settings.SERVER_URL + 'site/activation.html?userid=' + str(qtq_user.user.id) + '&hash=' + qtq_user.registration_token +'\'>' + settings.SERVER_URL + 'site/activation.html?userid=' + str(qtq_user.user.id) + '&hash=' + qtq_user.registration_token+ '</a> \
or simply copy and paste it into your browser address bar.</p> \
<p>You can then return to QuitTheQ to sign in.</p>\
<p>Please contact us on help@fuzzydigital.com if you have any problems signing in.</p><br>\
<p>Welcome to QuitTheQ!</p><br>\
t: @QuitTheQ<br />\
f: Facebook/QuitTheQ</p>'
                    #img_content_id = 'logo'
                    #img_data = open('media/images/logo.png', 'rb').read()
                
                    email = EmailMessage('Welcome to QuitTheQ', body, to=[qtq_user.user.username])
                    #email_embed_image(email, 'logo', img_data)
                    email.content_subtype = "html"
                    email.send()
    
    
#                    response = {
#                                    "username":qtq_user.user.username,
#                                    "first_name":qtq_user.user.first_name,
#                                    "email":qtq_user.user.email,
#                                    "active":qtq_user.user.is_active
#                                } 
                
                return {'message':"We have sent you an email. Please click on the link we sent you to make sure we have the correct email address. Then come back and get started!"}
                    
            
            else:
                return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '3', 'error_message':'Something went wrong while registering! Please try again.'})
        
        
        else:
            print "creating user"
#            try:
            user = User.objects.create_user(email, self.CONTENT['email'], password = self.CONTENT["password"])
        
            
            user.first_name = self.CONTENT['first_name']
            user.last_name = self.CONTENT['last_name']
            user.is_staff = False
            user.is_active = False
            user.is_superuser = False
            
            user.save()
        
            random_number = random.randrange(0, 10000)
            sha_key = sha.new(str(random_number))
            sha_digest = sha_key.hexdigest()
            
            qtq_user = UserProfile(user = user)

            qtq_user.registration_token = sha_digest
            venue_id = self.CONTENT['venue_id']
            venue_name = ""
            if venue_id != None and venue_id != "":    
                print "getting venue"
                venue_id = int(venue_id)
                qtq_user.venue_id = venue_id
                venue = Venue.objects.get(id = venue_id)
                if venue != None:
                    venue_name = venue.venue_name
            print "venue_name " + venue_name        
            qtq_user.venue_name = venue_name
            
            qtq_user.address = self.CONTENT['address']
            qtq_user.town_city = self.CONTENT['town_city']
            qtq_user.county_state = self.CONTENT['county_state']
            qtq_user.country_id = self.CONTENT['country_id']
            qtq_user.post_zipcode = self.CONTENT['post_zipcode']
            qtq_user.date_of_birth = self.CONTENT['date_of_birth']
            
            print "about to add the user group"
            user_group_id = self.CONTENT['user_group']
            print "we have a user_group: %s " % user_group_id

            user_group = UserGroup.objects.get(id = user_group_id)
            if user_group == None:
                user_group = UserGroup.objects.get(name = 'Master Admin')
            
            qtq_user.user_group = user_group
            print "about save an user %s" % qtq_user
            qtq_user.save()
            print "we have saved a user"

            body = '<p>Welcome to QuitTheQ.</p> \
<p>To activate your account just click on the link <a href=\'' + settings.SERVER_URL + 'site/activation.html?userid=' + str(qtq_user.user.id) + '&hash=' + qtq_user.registration_token +'\'>' + settings.SERVER_URL + 'site/activation.html?userid=' + str(qtq_user.user.id) + '&hash=' + qtq_user.registration_token+ '</a> \
or simply copy and paste it into your browser address bar.</p> \
<p>Website users will automatically be logged in, and QuitTheQ mobile app users can return to the Welcome screen to sign in.</p>\
<p>Please contact us on help@quittheq.co.uk if you have any problems signing in.</p><br>\
<p>Welcome to QuitTheQ!</p><br>\
<img src="http://qtq.fuzzydigital.com/img/logo.jpg" />'
            #img_content_id = 'logo'
            #img_data = open('media/images/logo.png', 'rb').read()
            
            email = EmailMessage('Welcome to QuitTheQ', body, to=[qtq_user.user.username])
            #email_embed_image(email, 'logo', img_data)
            email.content_subtype = "html"
            email.send()


            response = {
                            "username":qtq_user.user.username,
                            "first_name":qtq_user.user.first_name,
                            "email":qtq_user.user.email,
                            "active":qtq_user.user.is_active
                        } 
            
            return response
#            except:
#                return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '3', 'error_message':'Something went wrong while registering! Please try again.'})
            
class EditUserView(View):            
    
    form = EditUserProfileForm
    print "editing user really"
    def post(self, request, user_id):
        
        email = self.CONTENT['email']
#        first_name = self.CONTENT['first_name']
#        last_name = self.CONTENT['last_name']
#        password = self.CONTENT['password']
#         address = self.CONTENT['address']
#         town_city= self.CONTENT['town_city']
#         county_state= self.CONTENT['county_state']
#         country_id= self.CONTENT['country_id']
#         post_zipcode= self.CONTENT['post_zipcode']
#         date_of_birth= self.CONTENT['date_of_birth']
        
        print "we are here"

        if validateEmail(email) == False:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '5', 'error_message':'Invalid e-mail address'})
        
#        if first_name == None or len(first_name) == 0:
#            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '6', 'error_message':'First Name is not filled'})
#        
#        if last_name == None or len(last_name) == 0:
#            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '6', 'error_message':'Last Name is not filled'})
#        
#        if password == None or len(password) == 0:
#            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '7', 'error_message':'Password is not filled'})
        
        email = email.lower()
        user = User.objects.get(id = user_id)
        try:
            qtq_user = UserProfile.objects.get(user = user)
            print "we got a user"
        except:
            pass
            
        if qtq_user != None:
            # Update the Django User Record first
            if self.CONTENT['first_name'] != '' and self.CONTENT['first_name'] != user.first_name:
                user.first_name = self.CONTENT['first_name']
            if self.CONTENT['last_name'] != '' and self.CONTENT['last_name'] != user.last_name:
                user.last_name = self.CONTENT['last_name']                
            if email != '' and email != user.email:
                user.email = email 
            if self.CONTENT['password'] != None and self.CONTENT['password'] != '':
                user.set_password(self.CONTENT['password'])
            user.save()
            print "saved user"
            # Now update the QTQ User Profile
#            qtq_user = UserProfile(user = user)
            venue_id = self.CONTENT['venue_id']
            if venue_id != "":
                qtq_user.venue_id = venue_id
                venue = Venue.objects.get(id = venue_id)
                if venue != None:
                    qtq_user.venue_name = venue.venue_name
            else:
                qtq_user.venue_name = ""
                print "saved with no venue_name"
            if self.CONTENT['address'] != '' and self.CONTENT['address'] != qtq_user.address:
                qtq_user.address = self.CONTENT['address']
            if self.CONTENT['town_city'] != '' and self.CONTENT['town_city'] != qtq_user.town_city:
                qtq_user.town_city = self.CONTENT['town_city']
            if self.CONTENT['county_state'] != '' and self.CONTENT['county_state'] != qtq_user.county_state:
                qtq_user.county_state = self.CONTENT['county_state']
            if self.CONTENT['country_id'] != '' and self.CONTENT['country_id'] != qtq_user.country_id:
                qtq_user.country_id = self.CONTENT['country_id']
            if self.CONTENT['post_zipcode'] != '' and self.CONTENT['post_zipcode'] != qtq_user.post_zipcode:
                qtq_user.post_zipcode = self.CONTENT['post_zipcode']

            dob = self.CONTENT['date_of_birth']
            if dob != "":
                qtq_user.date_of_birth = dob
                
            if self.CONTENT['user_group'] != '' and self.CONTENT['user_group'] != qtq_user.user_group: 
                user_group = UserGroup.objects.get(id = self.CONTENT['user_group'])

            print "about to add the user group %s" % user_group

            qtq_user.user_group = user_group

            qtq_user.save()
            print "saved profile"

    
class UserRegisterView(View):  
    ''' 
    QTQ Users : Adds a User (a Django User will be automatically created)
    '''     
    
    form = AddUserProfileForm
    def post(self, request):
        
        #Field Validation
        email = self.CONTENT['email']
        first_name = self.CONTENT['first_name']
        last_name = self.CONTENT['last_name']
        password = self.CONTENT['password']
#         address = self.CONTENT['address']
#         town_city= self.CONTENT['town_city']
#         county_state= self.CONTENT['county_state']
#         country_id= self.CONTENT['country_id']
#         post_zipcode= self.CONTENT['post_zipcode']
#         date_of_birth= self.CONTENT['date_of_birth']
        
        print "we are here"

        if validateEmail(email) == False:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '5', 'error_message':'Invalid e-mail address'})
        
        if first_name == None or len(first_name) == 0:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '6', 'error_message':'First Name is not filled'})
        
        if last_name == None or len(last_name) == 0:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '6', 'error_message':'Last Name is not filled'})
        
        if password == None or len(password) == 0:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '7', 'error_message':'Password is not filled'})
            
        
        email = email.lower() 
        user = None
        
        try: 
            print "we are here 2"
            user = User.objects.get(email = email)
            
        except:
            pass
        
        
        if user != None:
            print "we have a user"
            qtq_user = None
            
            try:
                qtq_user = UserProfile.objects.get(user = user)
            except:
                pass
            
            
            if qtq_user != None:
                print "We have a user profile"
                if qtq_user.qtq_registered == False:
                        qtq_user.qtq_registered = True
                        qtq_user.save()
                        user.set_password(self.CONTENT['password'])
                        user.save()
                        
                        return {
                                    "username":qtq_user.user.username,
                                    "first_name":qtq_user.user.first_name,
                                    "email":qtq_user.user.email,
                                    "active":qtq_user.user.is_active
                                }
                
                elif qtq_user.qtq_registered == True:
                    return Response(status.HTTP_401_UNAUTHORIZED, {'error_code': '1', 'error_message':"Have you already registered with QuitTheQ? We already have an account for this email address. If you can't remember your password, please reset it."})
                
                else:
                    user.first_name = self.CONTENT['first_name']
                    user.last_name = self.CONTENT['last_name']
                    user.is_active = False
                    user.set_password(self.CONTENT['password'])
                    
                    user.save()
                    
#                     qtq_user.treasured_registered = True
#                    qtq_user.save()
#                    user.is_active = False
                    
                    random_number = random.randrange(0, 10000)
                    sha_key = sha.new(str(random_number))
                    sha_digest = sha_key.hexdigest()
                
                    qtq_user = UserProfile.objects.get(user = user)
                    qtq_user.qtq_registered = True
                    qtq_user.registration_token = sha_digest
                    qtq_user.address = self.CONTENT['address']
                    qtq_user.town_city = self.CONTENT['town_city']
                    qtq_user.county_state = self.CONTENT['county_state']
                    qtq_user.post_zipcode = self.CONTENT['post_zipcode']
                    qtq_user.country_id = self.CONTENT['country_id']
                    qtq_user.date_of_birth = self.CONTENT['date_of_birth']
                    user_group = UserGroup.objects.get(name = 'App')
                    qtq_user.user_group = user_group
                    
                    qtq_user.save()
                    
                    
                    
                    body = '<p>Hi, thank you for registering your QuitTheQ account, the next step is easy.</p> \
<p>To activate your account just click on the link <a href=\'' + settings.SERVER_URL + 'site/activation.html?userid=' + str(qtq_user.user.id) + '&hash=' + qtq_user.registration_token +'\'>' + settings.SERVER_URL + 'site/activation.html?userid=' + str(qtq_user.user.id) + '&hash=' + qtq_user.registration_token+ '</a> \
or simply copy and paste it into your browser address bar.</p> \
<p>You can then return to QuitTheQ to sign in.</p>\
<p>Please contact us on help@fuzzydigital.com if you have any problems signing in.</p><br>\
<p>Welcome to QuitTheQ!</p><br>\
t: @QuitTheQ<br />\
f: Facebook/QuitTheQ</p>'
                    #img_content_id = 'logo'
                    #img_data = open('media/images/logo.png', 'rb').read()
                
                    email = EmailMessage('Welcome to QuitTheQ', body, to=[qtq_user.user.username])
                    #email_embed_image(email, 'logo', img_data)
                    email.content_subtype = "html"
                    email.send()
    
    
#                    response = {
#                                    "username":qtq_user.user.username,
#                                    "first_name":qtq_user.user.first_name,
#                                    "email":qtq_user.user.email,
#                                    "active":qtq_user.user.is_active
#                                } 
                
                return {'message':"We have sent you an email. Please click on the link we sent you to make sure we have the correct email address. Then come back and get started!"}
                    
            
            else:
                return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '3', 'error_message':'Something went wrong while registering! Please try again.'})
        
        
        else:
            print "creating user"
#            try:
            user = User.objects.create_user(email, self.CONTENT['email'], password = self.CONTENT["password"])
        
            
            user.first_name = self.CONTENT['first_name']
            user.last_name = self.CONTENT['last_name']
            user.is_staff = False
            user.is_active = False
            user.is_superuser = False
            
            
            
            user.save()
        
            random_number = random.randrange(0, 10000)
            sha_key = sha.new(str(random_number))
            sha_digest = sha_key.hexdigest()
            
            qtq_user = UserProfile(user = user)
#             qtq_user.treasured_registered = True
            qtq_user.registration_token = sha_digest
            
            qtq_user.address = self.CONTENT['address']
            qtq_user.town_city = self.CONTENT['town_city']
            qtq_user.county_state = self.CONTENT['county_state']
            qtq_user.country_id = self.CONTENT['country_id']
            qtq_user.post_zipcode = self.CONTENT['post_zipcode']
            qtq_user.date_of_birth = self.CONTENT['date_of_birth']
            user_group = UserGroup.objects.get(name = 'App')
            qtq_user.user_group = user_group
            qtq_user.save()
            print "we have saved a user"

            body = '<p>Welcome to QuitTheQ.</p> \
<p>To activate your account just click on the link <a href=\'' + settings.SERVER_URL + 'site/activation.html?userid=' + str(qtq_user.user.id) + '&hash=' + qtq_user.registration_token +'\'>' + settings.SERVER_URL + 'site/activation.html?userid=' + str(qtq_user.user.id) + '&hash=' + qtq_user.registration_token+ '</a> \
or simply copy and paste it into your browser address bar.</p> \
<p>Website users will automatically be logged in, and QuitTheQ mobile app users can return to the Welcome screen to sign in.</p>\
<p>Please contact us on help@fuzzydigital.com if you have any problems signing in.</p><br>\
<p>Welcome to QuitTheQ!</p><br>\
<img src="http://qtq.fuzzydigital.com/img/logo.jpg" />'
            #img_content_id = 'logo'
            #img_data = open('media/images/logo.png', 'rb').read()
            
            email = EmailMessage('Welcome to QuitTheQ', body, to=[qtq_user.user.username])
            #email_embed_image(email, 'logo', img_data)
            email.content_subtype = "html"
            email.send()


            response = {
                            "username":qtq_user.user.username,
                            "first_name":qtq_user.user.first_name,
                            "email":qtq_user.user.email,
                            "active":qtq_user.user.is_active
                        } 
            
            return response
#            except:
#                return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '3', 'error_message':'Something went wrong while registering! Please try again.'})


            
def validateEmail(email):
    from django.core.validators import validate_email
    from django.core.exceptions import ValidationError
    try:
        validate_email( email )
        return True
    except ValidationError:
        return False
            
            
               
class UserActivateView(View):
    ''' 
    QTQ Users : Activates an inactive user
    '''
    
    def get(self, request, user_id, hash):
        
        qtq_user = None
        
        try:
            qtq_user = UserProfile.objects.get(user__id = user_id)
        except:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'User does not exist'})
            
        
        if qtq_user.user.is_active == False:
            
            if qtq_user.registration_token == hash:
            
                try:
                    user = qtq_user.user
                    user.is_active = True
                    user.save()
                
                    qtq_user.registration_token = None
                    qtq_user.save()
                    
                    return { 'message':'OK'}
                    
                except:
                    return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '2', 'error_message':'A Server error occurred while activating an user'})
        
                return  Response(status.HTTP_200_OK)
        
            else:
                return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '3', 'error_message':'User registration token doesn\'t match'})
            
        else: 
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '4', 'error_message':'User is already active'})
        
        

class UserLoginView(View): 
    ''' 
    Login : Authenticate and Login a Race User and a Django User
    '''              
    form = LoginForm
        
    def post(self, request):
        error = None
        
        username = self.CONTENT['username']
        password = self.CONTENT['password']
        user_django = authenticate(username=username, password=password)
        
        print "authenticated user: %s" % user_django
        
#        if user_django.is_active:
#            print "User is Active"
        
        if user_django is not None:
            if user_django.is_active:
                login(request, user_django)
                try:
                    user = UserProfile.objects.get(user = user_django)
                    print "got UserProfile:%s" % user
                except:
                    return Response(status.HTTP_401_UNAUTHORIZED, {"error_code":'1', "error_message":'Invalid username or password'})
#                return {"message":"Login"}
                return user
            else:
                return Response(status.HTTP_401_UNAUTHORIZED, {"error_code":'2', "error_message":"We haven't activated your account yet - check your emails for an activation message."})
        else:
            return Response(status.HTTP_401_UNAUTHORIZED, {"error_code":'3', "error_message":"We don't recognise that email address or password. Please check them and try again."})
        
class ResendActivation(View):
    
    def post(self, request):
        
        try:
            user = UserProfile.objects.get(user__username = self.CONTENT['email'])
            
            if user.registration_token is None or user.registration_token == '':
                return Response(status.HTTP_401_UNAUTHORIZED, {"error_code":'2', "error_message":'No activation code for this user'})
            
                                
            
            body = '<p>Hi, I\'d like to thank you for registering your QuitTheQ account, the next step is easy.</p> \
<p>To activate your account just click on the link <a href=\'' + settings.SERVER_URL + 'site/activation.html?userid=' + str(user.user.id) + '&hash=' + user.registration_token +'\'>' + settings.SERVER_URL + 'site/activation.html?userid=' + str(user.user.id) + '&hash=' + user.registration_token+ '</a> \
or simply copy and paste it into your browser address bar.</p> \
<p>You can return to the QuitTheQ app to sign in.</p>\
<p>Please contact us on help@fuzzydigital.com if you have any problems signing in.</p><br>\
<p>Welcome to QuitTheQ!</p><br>\
t: @QuitTheQ<br />\
f: Facebook/QuitTheQ</p>'
            #img_content_id = 'logo'
            #img_data = open('media/images/logo.png', 'rb').read()
                
            email = EmailMessage('Welcome to QuitTheQ', body, to=[user.user.username])
            #email_embed_image(email, 'logo', img_data)
            email.content_subtype = "html"
            email.send()
            
            
            return { "message":"OK"}
                
        except:
            return Response(status.HTTP_401_UNAUTHORIZED, {"error_code":'1', "error_message":'User Not Found'})
        
class ResetPassword(View):
    
    def post(self, request):
        
        try:
            user = UserProfile.objects.get(user__username = self.CONTENT['email'])
            
            seed = random.random()
            password_hash = sha.new(str(seed))
            
            user.password_token = password_hash.hexdigest()
            
            user.save()
            
            body = ' \
            <p>Hi,</p> \
            <p>It looks like you might have forgotten your password as we have received a request to reset it. To reset your password please click on this link: </p>\
            <p><a href=\'' + settings.SERVER_URL + 'site/password.html?hash=' + user.password_token + '\'>' + settings.SERVER_URL + 'site/password.html?hash=' + user.password_token  + '</a></p> \
            <p>If the link doesn\'t work you can simply copy and paste it into your browser address bar or email us on help@quittheq.co.uk for support.</p> \
            <p>If you don\'t need to reset your password then ignore this email.</p> \
            <p>Your account security is important so if you think there is a problem please contact us on help@quittheq.co.uk.</p> \
            <p>Kind Regards,</p>'
            
            #img_content_id = 'logo'
            #img_data = open('media/images/logo.png', 'rb').read()
                
            email = EmailMessage('QuitTheQ Password Reset', body, to=[user.user.username])
            #email_embed_image(email, 'logo', img_data)
            email.content_subtype = "html"
            email.send()
                
#            email = EmailMessage('Treasured - Password Reset', 'Hi, looks like you might have forgotten your password as we have received a request to reset it. If you are ready to proceed please click on this link ' + settings.SERVER_URL + 'site/password.html?hash=' + user.password_token + ' \n\n\
#If the link doesn\'t work you can simply copy and paste it into your browser address bar or email us on help@quittheq.co.uk for support.\n\n\
#If you don\'t need to reset your password then ignore this email.\n\n\
#Your account security is important so if you think there is a problem please contact us on help@treasured.com.\n\n\n\
#Amy Scott\n\
#CEO & Co-Founder, Treasured Ltd\n\
#t: @TreasuredOnline\n\
#f: Facebook/Treasured', to=[user.user.username])
#            email.send()
            

            return { "message":"OK"}
                
        except:
            return Response(status.HTTP_401_UNAUTHORIZED, {"error_code":'1', "error_message":'User Not Found'})

class AppResetPassword(View):
    
    def post(self, request):
        
        try:
            user_profile = UserProfile.objects.get(user__username = self.CONTENT['email'].lower())
#            user_profile.user.set_password(self.CONTENT['password'])
#            user_profile.user.is_active = False
            print "update user: %s" % user_profile
            random_number = random.randrange(0, 10000)
            sha_key = sha.new(str(random_number))
            sha_digest = sha_key.hexdigest()
            print "update"
#            user_profile = UserProfile.objects.filter(id = user_profile.id).update(is_active = False, registration_token = sha_digest  )
            user_profile.registration_token = sha_digest
            print "is_active: False"
            user = user_profile.user
            user.is_active = False 
            print "user profile updated"
#            user = User.objects.filter(id = user_profile.user_id).update(is_active = False  ).update(registration_token = sha_digest  )
            user.set_password(self.CONTENT['password'])
            print "password set"
            user.save()
            user_profile.save()

            body = ' \
            <p>Hi,</p> \
            <p>We have reset your password, to reactive your account please click on this link: </p>\
            <p><a href=\'' + settings.SERVER_URL + 'site/activation.html?userid=' + str(user_profile.user.id) + '&hash=' + user_profile.registration_token +'\'>' + settings.SERVER_URL + 'site/activation.html?userid=' + str(user_profile.user.id) + '&hash=' + user_profile.registration_token+ '</a>\
            <p>If the link doesn\'t work you can simply copy and paste it into your browser address bar or email us on help@quittheq.co.uk for support.</p> \
            <p>If you don\'t need to reset your password then ignore this email.</p> \
            <p>Your account security is important so if you think there is a problem please contact us on help@quittheq.co.uk.</p> \
            <p>Kind Regards,</p>'
            
            #img_content_id = 'logo'
            #img_data = open('media/images/logo.png', 'rb').read()
                
            email = EmailMessage('QuitTheQ Password Reset', body, to=[user_profile.user.username])
            #email_embed_image(email, 'logo', img_data)
            email.content_subtype = "html"
            email.send()
                
#            email = EmailMessage('Treasured - Password Reset', 'Hi, looks like you might have forgotten your password as we have received a request to reset it. If you are ready to proceed please click on this link ' + settings.SERVER_URL + 'site/password.html?hash=' + user.password_token + ' \n\n\
#If the link doesn\'t work you can simply copy and paste it into your browser address bar or email us on help@treasured.com for support.\n\n\
#If you don\'t need to reset your password then ignore this email.\n\n\
#Your account security is important so if you think there is a problem please contact us on help@treasured.com.\n\n\n\
#Amy Scott\n\
#CEO & Co-Founder, Treasured Ltd\n\
#t: @TreasuredOnline\n\
#f: Facebook/Treasured', to=[user.user.username])
#            email.send()
            

            return { "message":"OK"}
                
        except:
            return Response(status.HTTP_401_UNAUTHORIZED, {"error_code":'1', "error_message":'User Not Found'})
    
    
class ValidatePassword(View):
    
    def post(self, request, hash):
            
        print "validating password"
        qtq_user = None
        
        try:
            qtq_user = UserProfile.objects.get(password_token = hash)
        except:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'User does not exist'})
            
        
        
        
        password = self.CONTENT['password']
        django_user = qtq_user.user
        django_user.set_password(password)
        django_user.password_token = None
        django_user.save()
        
                    
        return { "message":"OK"}
    
        
        
class ChangePassword(View):
    
    permissions = (IsAuthenticated,)
    
    def post(self, request):
        
        django_user = get_user_from_request(request)
        
        if django_user.check_password(self.CONTENT['old_password']):
            django_user.set_password(self.CONTENT['new_password'])
            django_user.save()
            return { "message":"OK" }
        
        else:
            return Response(status.HTTP_400_BAD_REQUEST, {"error_code":'1', "error_message":'Error getting QuitTheQ user'})
      
class RegisterNotificationDevice(View):
    permissions = (IsAuthenticated,)
    
    def post(self, request):
        print "Registering Device"
        django_user = get_user_from_request(request)
        user_profile = UserProfile.objects.get(user = django_user)
        token = self.CONTENT['token']
        platform = self.CONTENT['device']
        print "Token: %s" % token
        print "Platform: %s" % platform
        
        if token != None:
            try:
#                 device = User_Device.objects.get(notification_token = token)
                device = User_Device.objects.get(user = user_profile)
                print "got device %s" % device
                if device.notification_token != token:
                    if platform == 'IOS':
                        try:
                            apnsdevice = APNSDevice.objects.get( user=django_user )
                            apnsdevice.delete()
                            print "deleted APNS token"
                            apnsdevice = APNSDevice.objects.create( user=django_user, registration_id=token )
                            apnsdevice.save()
                            print "saved APNS token"
                        except Exception, e:
                            print "%s" %  e
                            
                    elif platform == 'ANDROID':        
                        try:
                            gcmdevice = GCMDevice.objects.get( user=django_user )
                            gcmdevice.delete()
                            print "deleted GCM token"
                            gcmdevice = GCMDevice.objects.create( user=django_user, registration_id=token )
                            gcmdevice.save()
                            print "saved GCM token"
                        except Exception, e:
                            print "%s" %  e
                            
                    device.notification_token = token
                    device.platform = platform
                    device.save()
                    print "updated internal token"
    #                apnsdevice = APNSDevice.objects.get( user=django_user )
    #                apnsdevice.registration_id=token
    #                apnsdevice.save()
#                     if platform == 'IOS':                
#                         try:
#                             apnsdevice = APNSDevice.objects.create( user=django_user, registration_id=token )
#                             apnsdevice.save()
#                             print "saved APNS token"
#                         except Exception, e:
#                             print "%s" %  e
#                             
#                         print "saved APNS token" 
#                                        
#                     elif platform == 'ANDROID':                
#                         try:
#                             gcmdevice = GCMDevice.objects.create( user=django_user, registration_id=token )
#                             gcmdevice.save()
#                             print "saved GCM token"
#                         except Exception, e:
#                             print "%s" %  e
#                             
#                         print "saved GCM token"
                

                    
            except User_Device.DoesNotExist:
                print "User not registered for notifications"
                device = User_Device( user = user_profile,
                                      notification_token = token,
                                      platform = platform)
                device.save()
                print "saved internal token"
                try:

                    if platform == 'IOS': 
                        apnsdevice = APNSDevice.objects.create( user=django_user, registration_id=token )
                        apnsdevice.save()
                        print "saved APNS token"
                    elif platform == 'ANDROID':
                        gcmdevice = GCMDevice.objects.create( user=django_user, registration_id=token )
                        gcmdevice.save()
                        print "saved GCM token"
                
                except Exception, e:
                    print "%s" %  e
#            try:
#                user_token = User_Device.objects.get(notification_token = token)
#            except User_Device.DoesNotExist:
#                print "Token not already in use"
#                device = User_Device( user = user_profile,
#                                      notification_token = token)
#                device.save()
#                print "saved internal token"
#                apnsdevice = APNSDevice.objects.create( user=django_user, registration_id=token )
#                apnsdevice.save()
#                print "saved APNS token"
            
            return { "message":"OK" }
        
        else:
            return Response(status.HTTP_400_BAD_REQUEST, {"error_code":'1', "error_message":'Error adding the token'})
      
    
    
class UserDetailView(View):  
    ''' 
    Gets User Profile Settings
    '''           
    form = EditUserProfileForm
#     permissions = (IsAuthenticated,)
    
    def get(self, request):
        print "The Request %s" % request
        
        try:
            user = get_user_from_request(request)
            print "got a user from the request %s" % user
            user_profile = UserProfile.objects.get(user = user)
            print "got a userProfile from the user %s" % user
            return user_profile
#            return {
#                        "first_name":user.first_name,
#                        "last_name":user.last_name,
#                        "email":user.email,
#                        "id":user.id,
#                        "venue_id":user_profile.venue_id
#                    }
        except:
            return None  

    
    def post(self, request):  
        user = None        
       
        try:
            reset_password = True
            
            first_name = self.CONTENT['first_name']
            last_name = self.CONTENT['last_name']
            email = self.CONTENT['email']
            password = self.CONTENT['password']
            
            if validateEmail(email) == False:
                return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'Invalid e-mail address'})
        
            if first_name == None or len(first_name) == 0:
                return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '2', 'error_message':'First Name is not filled'})
        
            if last_name == None or len(last_name) == 0:
                return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '3', 'error_message':'Last Name is not filled'})
        
            if password == None or len(password) == 0:
                reset_password = False
                
            
            user = get_user_from_request(request)
            
            user.first_name = first_name
            user.last_name = last_name
            user.email = email
            user.username = email
            
            if reset_password:
                user.set_password(password)
                
            user.save()
            
            qtq_user = UserProfile.objects.get(user = user)
            
            qtq_user.save()
            
            return { 'message':'OK'    
                    }
            
        except Exception, e:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '4', 'error_message':'Error occurred while editing profile'})
    
class UsersView(View):
    
    form = UserProfileForm
    permissions = (IsAuthenticated,)

    def get(self, request, group_id):
        print "we re here"
#        group_id =  self.CONTENT['user_group']
        print "looking for group: %s" % group_id
        if group_id != "99":            
            try:
                user = get_user_from_request(request)
                userProf = UserProfile.objects.get(user = user)
                print "got a profile "
                user_group = UserGroup.objects.get(id = group_id)
                print "got a user group " 
                venue_id = userProf.venue_id
                print "venue id = %s" % venue_id
                if venue_id != None:
                    users = UserProfile.objects.filter(user_group=user_group, venue_id = venue_id).order_by('-user__username') 
                else:
                   users = UserProfile.objects.filter(user_group=user_group).order_by('venue_name', 'user__username')
#                users = UserProfile.objects.filter
                return users        
            except:
                 return {"message":"Can't find users"}   
        else:
            try:
    #            users = UserProfile.objects.filter(user_group="1") 
                user = get_user_from_request(request)
                userProf = UserProfile.objects.get(user = user)
                venue_id = userProf.venue_id
#                print "venue id = " + venue_id
                if venue_id != None:
                    users = UserProfile.objects.filter(venue_id = venue_id).order_by('-user__username')
                else:
                    users = UserProfile.objects.all().annotate(null_venue_name=Count('venue_name')).order_by('-null_venue_name', 'venue_name')

#                    users = UserProfile.objects.filter().order_by('venue_name', 'user__username')

#                users = UserProfile.objects.filter
                return users        
            except:
                 return {"message":"Can't find users"}   

         
class UserLogoutView(View): 
    ''' 
    Logout : Logout a Race User and a Django User
    ''' 
#    permissions = (IsAuthenticated, )   
    
    def get(self, request): 
        try:
            logout(request) 
            return {'message':'Logout'}
        except:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while logging out'})    


class CompanyUsersView(View):
    
    form = UserProfileForm
    permissions = (IsAuthenticated,)

    def get(self, request, company_id):
        if company_id != "":            
            try:
                users = UserProfile.objects.filter(company_id=company_id).order_by('-user__username')
                return users        
            except:
                 return {"message":"Can't find users"}   
        else:
            try:
    #            users = UserProfile.objects.filter(user_group="1") 
                users = UserProfile.objects.filter().order_by('company_id', 'user__username')
                return users        
            except:
                 return {"message":"Can't find users"}   
           

class UserView(View):

    permissions = (IsAuthenticated,)
    
    def get(self, request, user_id):
#        user = User.objects.get(id = user_id)         
        user_profile = UserProfile.objects.get(id = user_id)    
        if user_profile != None:
            return user_profile
        else:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while fetching user'})                      

class ResetUserView(View):

#    permissions = (IsAuthenticated,)
    
    def post(self, request):
        user_profile = None
        
        user_name = self.CONTENT['user_name'].lower()
        user = User.objects.get(username = user_name)         
        user_profile = UserProfile.objects.get(user = user)    
        if user_profile != None:
            return user_profile
        else:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while fetching user'})                      

class UserDeleteView(View):

    permissions = (IsAuthenticated,)

    def get(self, request, user_id):
        print "Delete User"
        user = User.objects.get(id=user_id)
            
        if user != None:
            user.delete()

            return {"message":"User Deleted"}
        else:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while deleting the user'})                      

class UserSetActiveView(View):

    permissions = (IsAuthenticated,)

    def get(self, request, user_id):
        print "Activating User"
        try:
            userprofile = UserProfile.objects.get(id=user_id)
            user = User.objects.filter(id = userprofile.user_id).update(is_active = True  )       
            print "activating user " + userprofile.user.username    
            return {"message":"User Activated"}
        except:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while activating the user'})                      

class UserSetDeactiveView(View):

    permissions = (IsAuthenticated,)

    def get(self, request, user_id):
        print "Deactivating User"
        try:
            userprofile = UserProfile.objects.get(id=user_id)
            user = User.objects.filter(id = userprofile.user_id).update(is_active = False  )       
            print "deactivating user " + userprofile.user.username    
            return {"message":"User Deactivated"}
        except:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while deactivating the user'})                      

         


####### COMPANY VIEWS
    
class AddNewCompanyView(View):  
    ''' 
    Companies : Adds a Company
    '''     
    
    form = AddCompanyForm
    permissions = (IsAuthenticated,)

    def post(self, request):
        
        #Field Validation
        email = self.CONTENT['email']
        company_name = self.CONTENT['company_name']
        country_id = self.CONTENT['country_id']
#        print "we are here: " + email + ' ' + venue_name + ' ' + country_id

        if email != "" and validateEmail(email) == False:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '5', 'error_message':'Invalid e-mail address'})
        
        if company_name == None or len(company_name) == 0:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '6', 'error_message':'Venue Name is not filled'})
        
        if country_id != None:
            country = Country.objects.get(id = country_id)

     
        email = email.lower() 
        company = None
        
        try: 
            print "we are here 2"
            company = Company.objects.get(email = email)
            
        except:
            pass
        
        
        if company != None:
            return Response(status.HTTP_401_UNAUTHORIZED, {'error_code': '1', 'error_message':"Venue has already registered with QuitTheQ? We already have an account for this email address. "})

        else:
#            venue = Venue.objects.create_venue(venue_name, email)
            # We are going to creat the Venue Admin User from the Contact Information
            contact_email = self.CONTENT['contact_email']
            print "email:" + contact_email
            if contact_email != None:
                user = None
                if validateEmail(contact_email) == False:
                    return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '5', 'error_message':'Invalid Contact e-mail address'})
                
                try: 
                    user = User.objects.get(email = contact_email)
                    print "got: %s" % user
                except:
                    pass
                
                if user != None:
                    print "we already have that user"
                    return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':"User has already registered with QuitTheQ? We already have an account for this email address. "})

#                user = User.objects.get()
                user = User.objects.create_user(contact_email, contact_email, password = "admin")
                user.first_name = self.CONTENT['contact_first_name']
                user.last_name = self.CONTENT['contact_last_name']
                user.is_staff = False
                user.is_active = False
                user.is_superuser = False
                user.save()
            
                random_number = random.randrange(0, 10000)
                sha_key = sha.new(str(random_number))
                sha_digest = sha_key.hexdigest()

                lf_user = UserProfile(user = user)
                lf_user.liveflyte_registered = True
                lf_user.registration_token = sha_digest
                user_group = UserGroup.objects.get(name = 'Company Admin')
                lf_user.user_group = user_group
#                qtq_user.venue_id = venue.id
#                qtq_user.address = self.CONTENT['address']
#                qtq_user.town_city = self.CONTENT['town_city']
#                qtq_user.county_state = self.CONTENT['county_state']
#                qtq_user.country_id = self.CONTENT['country_id']
#                qtq_user.post_zipcode = self.CONTENT['post_zipcode']
#                qtq_user.date_of_birth = self.CONTENT['date_of_birth']
       
                lf_user.save()
            
            if email == "" and contact_email != "":
                email = contact_email
                    
            company = Company(company_name = self.CONTENT['company_name'],
                        address1= self.CONTENT['address1'],
                        town_city= self.CONTENT['town_city'],
                        county_state= self.CONTENT['county_state'],
                        post_zipcode= self.CONTENT['post_zipcode'],
                        country_id = country,
                        phone= self.CONTENT['phone'],
                        email= self.CONTENT['email'],
                        website= self.CONTENT['website'],
#                        contact_name= self.CONTENT['contact_name'],
                        contact = lf_user,
                        contact_phone= self.CONTENT['contact_phone'],
#                        contact_email= self.CONTENT['contact_email'],
                        contact_title= self.CONTENT['contact_title'],
                        state_indicator= 0
            )
            company.save()
#           Add Venue to the UserProfile
            lf_user.company_id = company.id
            lf_user.company_name = company.company_name
            lf_user.save()                    
            return {'message':"We have sent you an email. Please click on the link we sent you to make sure we have the correct email address. Then come back and get started!"}

class EditCompanyView(View):  
    ''' 
    QTQ Venues : Edit a Venue
    '''     
    
#    form = EditVenueForm
    permissions = (IsAuthenticated,)

    def post(self, request, company_id):
        print "Editing Company"
        #Field Validation
        email = self.CONTENT['email']
        print "Got email"
        company_name = self.CONTENT['company_name']
        print "got company_name"
        country_id = self.CONTENT['country_id']
        print "we are here"

        if validateEmail(email) == False:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '5', 'error_message':'Invalid e-mail address'})
        
        if company_name == None or len(company_name) == 0:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '6', 'error_message':'Company Name is not filled'})
        
        if country_id != None:
            country = Country.objects.get(id = country_id)

        print "now here"
        email = email.lower() 
        company = None
        
        try: 
            print "we are here 2"
            venue = Company.objects.get(id = company_id)
            print "we are here 3 - "
        except:
            pass
        
        
        if company != None:
            contact = UserProfile.objects.get(id = company.contact.id)
            user = User.objects.filter(id = contact.user.id).update( first_name= self.CONTENT['contact_first_name'],
                                                                         last_name= self.CONTENT['contact_last_name'])
            
            company = Company(id = venue_id,
                        company_name = self.CONTENT['company_name'],
                        address1= self.CONTENT['address1'],
                        town_city= self.CONTENT['town_city'],
                        county_state= self.CONTENT['county_state'],
                        post_zipcode= self.CONTENT['post_zipcode'],
                        country_id = country,
                        phone= self.CONTENT['phone'],
                        email= self.CONTENT['email'],
                        website= self.CONTENT['website'],
                        contact = contact,
                        contact_phone= self.CONTENT['contact_phone'],
                        contact_email= self.CONTENT['contact_email'],
                        contact_title= self.CONTENT['contact_title']
            )
            company.save()
            return {"message":"Company updated."}
        else:
            return {'message':"There was a problem updating the company"}
 
class CompanyView(View):

    permissions = (IsAuthenticated,)

    def get(self, request):
#        print request.session
        user = get_user_from_request(request)
        companies = Company.objects.all().order_by('company_name')         
        print "Getting Companies"   
        result = []
#        venue = None
#           
#        for v in venues:
#            venue = Venue.objects.get(venue_name = v)
#            result.append(venue)
        return companies                      

class ActiveVenuesView(View):

    permissions = (IsAuthenticated,)

    def get(self, request):
#        print request.session
        user = get_user_from_request(request)
        venues = Venue.objects.filter(state_indicator=1).order_by('venue_name')    
        print "Getting Venues"   
        result = []
#        venue = None
#           
#        for v in venues:
#            venue = Venue.objects.get(venue_name = v)
#            result.append(venue)
        return venues                      


class ThisCompanyView(View):

    permissions = (IsAuthenticated,)

    def get(self, request):
#        print request.session
        user = get_user_from_request(request)
        userPro = UserProfile.objects.get(user = user)
        company = Company.objects.get( id = userPro.company_id)         
        print "Getting Company"   
        result = []

        return company                      

 
class CompanyDetailView(View):

    permissions = (IsAuthenticated,)

    def get(self, request, company_id):
        print "we re here"
        company = Company.objects.get(id = company_id)         
            
        print "we re here5" 
        if company != None:
            return company
        else:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while getting company'})                      
               
class CompanyDeleteView(View):

    permissions = (IsAuthenticated,)

    def get(self, request, company_id):
        print "we re here"
        company = Company.objects.get(id = company_id)                 
        users = UserProfile.objects.filter(company_id = company.id)

        if company != None:
            company.delete()
            
            return {"message":"Company Deleted"}
        else:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while deleting company'})                      

class CompanyActivateView(View):

    permissions = (IsAuthenticated,)

    def get(self, request, company_id):
        print "we re here"
        company = Company.objects.get(id = company_id)         
            
        print "we re here5" 
        if company != None:
            company.state_indicator = 1
            company.save()
            return {"message":"Venue Activated"}
        else:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while activating the company'})                      

class CompanyDeactivateView(View):

    permissions = (IsAuthenticated,)

    def get(self, request, company_id):
        print "we re here"
        company = Company.objects.get(id = company_id)         

        print "we re here5" 
        if company != None:
            company.state_indicator = 0
            company.save()
            return {"message":"company Dectivated"}
        else:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while de-activating the company'})                      



###### GENERAL    

def email_embed_image(email, img_content_id, img_data):
    """
    email is a django.core.mail.EmailMessage object
    """
    img = MIMEImage(img_data)
    img.add_header('Content-ID', '<%s>' % img_content_id)
    img.add_header('Content-Disposition', 'inline')
    email.attach(img)


def get_user_from_request(request):
    '''
    Get user from REST request
    '''
    request_user = None
#    print request
    if request.session.session_key is not None:
        try:
#            print request
            session = Session.objects.get(session_key=request.session.session_key)
            uid = session.get_decoded().get('_auth_user_id')
            request_user = User.objects.get(pk=uid)
            print "Got User From the request: %s" % request_user
        except:
            pass
    return request_user

###### CALCULATE DISTANCE
def distance_on_unit_sphere(lat1, long1, lat2, long2):

    # Convert latitude and longitude to 
    # spherical coordinates in radians.
    degrees_to_radians = math.pi/180.0
        
    # phi = 90 - latitude
    phi1 = (90.0 - lat1)*degrees_to_radians
    phi2 = (90.0 - lat2)*degrees_to_radians
        
    # theta = longitude
    theta1 = long1*degrees_to_radians
    theta2 = long2*degrees_to_radians
        
    # Compute spherical distance from spherical coordinates.
        
    # For two locations in spherical coordinates 
    # (1, theta, phi) and (1, theta, phi)
    # cosine( arc length ) = 
    #    sin phi sin phi' cos(theta-theta') + cos phi cos phi'
    # distance = rho * arc length
    
    cos = (math.sin(phi1)*math.sin(phi2)*math.cos(theta1 - theta2) + 
           math.cos(phi1)*math.cos(phi2))
    arc = math.acos( cos )

    # Remember to multiply arc by the radius of the earth 
    # in your favorite set of units to get length.
    return arc

def distance(origin, destination):
    lat1, lon1 = origin
    lat2, lon2 = destination
    radius = 6371 # km
 
    dlat = math.radians(lat2-lat1)
    dlon = math.radians(lon2-lon1)
    a = math.sin(dlat/2) * math.sin(dlat/2) + math.cos(math.radians(lat1)) \
        * math.cos(math.radians(lat2)) * math.sin(dlon/2) * math.sin(dlon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = radius * c
 
    return d

###### COUNTRIES VIEW 
class CountriesView(View):

    def get(self, request):
        print "Getting Countries"
        user = get_user_from_request(request)
        print "CountriesView: ot a user from the request: %s" % user
        try:
            countries = Country.objects.all()         
            return countries
        except:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while fetching countries'})                      

###### USER GROUP VIEWS
class UserGroupsView(View):

#     permissions = (IsAuthenticated,)
    def get(self, request):
        print "Getting User_Groups"
        try:
            groups = UserGroup.objects.all()         
            return groups
        except:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while fetching groups'})  
       
                        
###### FLIGHTS VIEWS
class GetFlightsView(View):

    permissions = (IsAuthenticated,)

    def get(self, request):
        print "Getting Flights"
        try:
            user = get_user_from_request(request)
            userProfile = UserProfile.objects.get(user = user)
            
            if (userProfile.user_group.name!="Site Admin"):
                company = userProfile.company
                flightList = Flight.objects.filter(company = company).exclude( latitude__isnull=True )       
            else:            
                flightList = Flight.objects.all().exclude( latitude__isnull=True )       
              
            return flightList
        except:
            
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while fetching flights'})
   
   
class GetFlightsShortView(View):

    permissions = (IsAuthenticated,)

    def get(self, request):
        print "Getting Short Flights"
        try:
            user = get_user_from_request(request)
            userProfile = UserProfile.objects.get(user = user)
            
            if (userProfile.user_group.name!="Site Admin"):
                company = userProfile.company
                flightList = Flight.objects.filter(company = company).exclude( latitude__isnull=True ).values('company__id','company__company_name','id','craft__id','flight_no', 'craft__aircraft_name', 'description', 'flight_date', 'latitude', 'longitude', 'start_flight', 'end_flight', 'state_indicator')       
            else:            
                flightList = Flight.objects.values('company__id','company__company_name','id','craft__id','flight_no', 'craft__aircraft_name', 'description', 'flight_date', 'latitude', 'longitude', 'start_flight', 'end_flight', 'state_indicator').exclude( latitude__isnull=True )      
            
                    
            return flightList
        except:
            
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while fetching flights'})
   

 ###### COMPANIES VIEWS
class GetCompaniesView(View):

    permissions = (IsAuthenticated,)

    def get(self, request):
        print "Getting Companies"
        try:
            companies = Company.objects.all()
              
            return companies
        except:
            
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while fetching companies'})
   
class GetCompanyView(View):

    permissions = (IsAuthenticated,)

    def get(self, request, comapnyid):
        print "Getting a company"
        try:
            company = Company.objects.get(id = companyid)
            print "Got Company : %s" % company.id
              
            return company
        except:
            
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while fetching a company'})
   

                                                        
###### LOGS VIEWS
class GetLogsView(View):

    permissions = (IsAuthenticated,)

    def get(self, request, craftid, flightid):
        print "Getting Logs for a flight"
        try:
#         find Flight from flightno
#         find logs where flight = flight
            craft = AirCraft.objects.get(id = craftid)
            print "Got Craft : %s" % craft.id
            flight = Flight.objects.get(craft = craft, id = flightid)
            print "Got Flight: %s" % flight.flight_no
            logList = Log.objects.filter( flight = flight ).order_by('log_no')       
              
            return logList
        except:
            
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while fetching logs'})
   
   
   
####### Log Flight Views   

def my_random_string(string_length=13):
    """Returns a random string of length string_length."""
    random = str(uuid.uuid4()) # Convert UUID format to a Python string.
    random = random.upper() # Make all characters uppercase.
    random = random.replace("-","") # Remove the UUID '-'.
    return random[0:string_length] # Return the random string.
# url(r'^getNewConfig/$', GetNewConfigView.as_view(), name='getNewConfigView'), #---

class GetNewConfigView(View):

#     permissions = (IsAuthenticated,)

    def get(self, request):
        print "Assign a new AppID"
        try:
            appid = my_random_string()
            print "AppID:" + appid
            box = BlackBox(appid = appid)
#             print "BlackBox:" + box
            box.save() 
            return Response( status.HTTP_200_OK, {'OK':'1','APPID': appid, 'PIN':'0000' })
        except:
            return Response( status.HTTP_400_BAD_REQUEST, {'OK': '0'})



# url(r'^logon/(?P<craft_id>\d+)/$', LogonView.as_view(), name='logonView'), #---
class LogonView(View):

#     permissions = (IsAuthenticated,)

    def get(self, request, appid):
        print "Starting a flight"
        try:
            print "Getting BlackBox for appid:" + appid
            box = BlackBox.objects.get(appid = appid)    
            print "got BlackBox: " + box.appid
            try:
                print "check for open flights"
                flights = Flight.objects.filter( craft = box.craft, state_indicator = 0).order_by('-id')
#                 loop through open flights for this blackbox and close them
                for flight in flights:
                    flight.state_indicator = 1
                    flight.save()
            except:
                print "error closing flights"
                
            flight_no = Flight.objects.count()
#             lastFlight = Flight.objects.all().order_by("-flight_no")[0]
#             flight_no = lastFlight.flight_no
#             print "Last flight no: %s" % flight_no
            flight_no = flight_no + 1
#             print "Creating flight no: %s" % flight_no
            flight = Flight(
                        flight_no = flight_no,
                        flight_date = timezone.now(),
                        company  = box.craft.company,
                        craft = box.craft,
                        start_flight = timezone.now(),
                        insert_point = timezone.now())
            print "flight object created"
            flight.save() 
            print "flight saved"
            return Response(status.HTTP_200_OK, {'OK':'1','FID': flight.id })
        except:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while starting a flight'})

# url(r'^battery/(?P<craft_id>\d+)/(?P<batt>\d+)/$', RegisterBatteryView.as_view(), name='registerBatteryView'), #---
class RegisterBatteryView(View):

#     permissions = (IsAuthenticated,)

    def get(self, request, appid, flightid, battid):
        print "Register Battery"
        try:
#             flightList = Flight.objects.all()   
                
            print "Getting BlackBox for appid:" + appid
            box = BlackBox.objects.get(appid = appid)    
            print "got BlackBox: %s" % box.appid
            flight = Flight.objects.get(pk = flightid )
            print "got Flight: %s" % flight.flight_no
            try:
                battery = Battery.objects.get(serial_no = battid )
                print "got Battery: %s" % battery.serial_no
#                 Check if battery registered to this company
                if battery.company != box.craft.company:
                    return Response(status.HTTP_200_OK, {'OK':'0' })
                else:
                    print "Battery is registered to %s" % battery.company.company_name
            except Battery.DoesNotExist:
                print "Battery not registered"
           
                battery = Battery(  company = box.craft.company,
                                    serial_no = battid,
                                    registered_date = timezone.now(),
                                    insert_point = timezone.now()
                )
                print "battery object created"
                battery.save() 
                print "battery saved"
            
            try:
#                 Now add this battery to the flight
#                 First check to see if there is a flightBattery object already, i.e. an inprogress flight

                try:
                    print "Now check for flightBattery record 2"
                    flightBattery = FlightBattery.objects.get(flight = flight)
                    print "Got a FlightBattery record"
                except FlightBattery.DoesNotExist:
                    print "No FlightBattery record"
                    flightBattery = FlightBattery(  flight = flight,
                                                    insert_point = timezone.now())
                    print "flightBattery object created"
                    flightBattery.save()
                    print "flightBattery record created"
                
#                 Now we can add this battery to the flightBattery record
                flightBattery.battery.add(battery)
                print "flightBattery saved"
                
            except:
                return Response(status.HTTP_200_OK, {'OK':'0' })
            
            return Response(status.HTTP_200_OK, {'OK':'1' })

        except:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while registering battery'})



#   log/(?P<craft_id>\d+)/(?P<alti>\d+)/(?P<lat>\d+)/(?P<long>\d+)/(?P<timestamp>\d+)/$
class LogFlightsView(View):

#     permissions = (IsAuthenticated,)
#     def post(self, request, appid, lat, lon, alti, timestamp):
    def get(self, request, appid, lat, lon, alti):
        print "Logging Flight: AppId:"+ appid +"  LAT: " + lat + " LONG: "+ lon +" ALTI: " + alti
        try:
#             flightList = Flight.objects.all()   
                
            print "Getting BlackBox for appid:" + appid
            box = BlackBox.objects.get(appid = appid)    
            print "got BlackBox: " + box.appid
            print "get the current flight"
            flight = Flight.objects.filter( craft = box.craft, state_indicator = 0).order_by('-id')[0]
            currentlog = 1
            try:
                log = Log.objects.filter( flight = flight).order_by('-log_no')[0]
                print "last log : %s" % log.log_no
                currentlog = log.log_no + 1
            except:
                print "first log" 
                
                flight.latitude = lat
                flight.longitude = lon
                flight.save()
                
#             print "got Flight No :%s" % flight.flight_no
            
            
            log = Log(  flight = flight,
                        log_no = currentlog,
                        latitude = lat,
                        longitude = lon,
                        altitude = alti,
                        insert_point = timezone.now()
                        
            )
            log.save()
            return Response(status.HTTP_200_OK, {'OK':'1','LOGID': log.id })

        except:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while logging flight'})
  
  
# url(r'^bye/(?P<craft_id>\d+)/$', CloseFlightLogView.as_view(), name='closeFlightLogView'), #--- 
class CloseFlightLogView(View):

#     permissions = (IsAuthenticated,)

    def get(self, request, appid):
        print "Close Flight Log"
        try:
            print "Getting BlackBox for appid:" + appid
            box = BlackBox.objects.get(appid = appid)    
            print "got BlackBox: " + box.appid
            print "get the current flight"
            flight = Flight.objects.filter( craft = box.craft, state_indicator = 0).order_by('-id')[0]
            print "got Flight No :%s" % flight.flight_no
            flight.state_indicator = 1
            flight.end_flight = timezone.now()
            flight.save()
            
            return Response(status.HTTP_200_OK, {'OK':'1'})
            
        except:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while finishing flight'})
   
     
   
   
   
   
   
   
   
   
   
###### PILOTS VIEWS
class GetPilotsView(View):

    permissions = (IsAuthenticated,)

    def get(self, request):
        print "Getting Pilots"
        try:
#             pilotList = Staff.objects.all()    
            user = get_user_from_request(request)
            userProfile = UserProfile.objects.get(user = user)
            role = Role.objects.get(title="Pilot")  
            
            if (userProfile.user_group.name!="Site Admin"):
                company = userProfile.company
                pilotList = Staff.objects.filter(role=role, company=company) 
            
            else:            
                pilotList = Staff.objects.filter(role=role)   

            return pilotList
        except:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while fetching Pilots'})
   

###### CRAFT VIEWS
class GetCraftView(View):

    permissions = (IsAuthenticated,)

    def get(self, request):
        print "Getting Craft"
        try:
            user = get_user_from_request(request)
            userProfile = UserProfile.objects.get(user = user)
                        
            if (userProfile.user_group.name!="Site Admin"):
                company = userProfile.company
                craftList = AirCraft.objects.filter(company=company)
            else:
                craftList = AirCraft.objects.all()        
            
            return craftList
        except:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while fetching Craft'})
   


###### PEOPLE VIEWS
class GetPeopleView(View):

    permissions = (IsAuthenticated,)

    def get(self, request):
        print "Getting People"
        try:
            user = get_user_from_request(request)
            userProfile = UserProfile.objects.get(user = user)
            
            if (userProfile.user_group.name!="Site Admin"):
                company = userProfile.company
                peopleList = Staff.objects.filter(company = company)       
            else:   
                peopleList = Staff.objects.all()      
              
            return peopleList
        except:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while fetching People'})

###### BLACKBOX VIEWS
class GetBlackBoxesView(View):

    permissions = (IsAuthenticated,)

    def get(self, request):
        print "Getting BlackBoxes"
        try:
            user = get_user_from_request(request)
            userProfile = UserProfile.objects.get(user = user)
                        
            if (userProfile.user_group.name!="Site Admin"):
                company = userProfile.company
                boxList = BlackBox.objects.filter(craft__company=company)
            else:    
                boxList = BlackBox.objects.all()       
              
            return boxList
        except:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while fetching BlackBoxes'})
   

###### BATTERIES VIEWS
class GetBatteriesView(View):

    permissions = (IsAuthenticated,)

    def get(self, request):
        print "Getting Batteries"
        try:
            user = get_user_from_request(request)
            userProfile = UserProfile.objects.get(user = user)
                        
            if (userProfile.user_group.name!="Site Admin"):
                company = userProfile.company_id
                batteryList = Battery.objects.filter(company=company)
            else:
                batteryList = Battery.objects.all()       
              
            return batteryList
        except:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while fetching Batteries'})
   

###### FLIGHT BATTERY VIEWS
class GetFlightBatteriesView(View):

    permissions = (IsAuthenticated,)

    def get(self, request, craftid, flightid):
        print "Getting Flight Batteries"
        try:
#         find Flight from flightno
#         find logs where flight = flight
            craft = AirCraft.objects.get(id = craftid)
            print "Got Craft : %s" % craft.id
            flight = Flight.objects.get(craft = craft, id = flightid)
            print "Got Flight: %s" % flight.flight_no
            batteryList = FlightBattery.objects.filter( flight = flight )       

            return batteryList
        except:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while fetching Flight Batteries'})
   


###### INCIDENTS VIEWS
class GetIncidentsView(View):

    permissions = (IsAuthenticated,)

    def get(self, request):
        print "Getting Incidents"
        try:
            flightList = Flight.objects.all()       
              
            return flightList
        except:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while fetching Incidents'})
   


    

###### STAFF VIEWS
class StaffMembersView(View):

#     permissions = (IsAuthenticated,)

    def get(self, request, company_id):
        print "Getting Members of staff"
        try:
            if company_id == "":
                staffList = Staff.objects.all()       
            else:
                company = Company.objects.get(id = venue_id)
                staffList = Staff.objects.filter(venue = venue)        
            return staffList
        except:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while fetching members of staff'})
    

    def post(self, request, company_id):        
        print "Adding a member of staff"
        try:
            company = Company.objects.get(company_id = company_id)
            staff = Staff(    name = self.CONTENT['name'],
                              company = company,
                              state_indicator = 0)         
            return  {'message':"Staff memeber added"}
        except:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while adding the members of staff'})


class EditStaffMemberView(View):
    permissions = (IsAuthenticated,)
    def post(self, request, staff_id):        
        print "Edit a member of staff"
        try:
            staff = Staff.objects.filter(id = staff_id).update(name = self.CONTENT['name'])
            return  {'message':"Staff memeber editted"}
        except:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while adding the members of staff'})

class StaffView(View):

    permissions = (IsAuthenticated,)

    def get(self, request, staff_id):
        print "Getting Member of staff"
        try:
            staff = Staff.objects.get(id = staff_id)       
            return staff
        except:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while fetching a member of staff'})
    
class StaffDeleteView(View):

    permissions = (IsAuthenticated,)

    def post(self, request, staff_id):
        print "we re here"
        staff = Staff.objects.get(id = staff_id)         
            
        if staff != None:
            staff.delete()
            #venue.save()
            return {"message":"staff Deleted"}
        else:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while deleting the staff'})                      

class StaffSetActiveView(View):

    permissions = (IsAuthenticated,)

    def get(self, request, staff_id):
        print "Activating staff"
        try:
            staff = Staff.objects.filter(id=staff_id).update(state_indicator = 1)
             
            return {"message":"staff Activated"}
        except:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while activating the staff'})                      

class StaffSetDeactiveView(View):

    permissions = (IsAuthenticated,)

    def get(self, request, staff_id):
        print "Deactivating staff"
        try:
            staff = Staff.objects.filter(id=staff_id).update(state_indicator = 0)
                 
            return {"message":"staff Deactivated"}
        except:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while deactivating the staff'})                      
        
class AddStaffMember(View):
    permissions = (IsAuthenticated,)
     
    def post(self, request, company_id):        
        print "Adding a member of staff %s " % company_id
        try:
            company = Company.objects.get(id = company_id)
            print "Got company: %s" % company.company_name
            print "Adding %s " % self.CONTENT['name']
            staff = Staff(    name = self.CONTENT['name'],
                              company = company,
                              state_indicator = 0)         
            staff.save()
            return  {'message':"Staff memeber added"}
        except:
            return Response(status.HTTP_400_BAD_REQUEST, {'error_code': '1', 'error_message':'An error occurred while adding the members of staff'})
        

        
def sendNotification( user, order_id ):
    print "Sending notification"
    user_device = User_Device.objects.get(user__id = user.id)
    #user_device = Device.objects.filter( user = user)
    user_token = user_device.notification_token
    print "User Token: %s " %  user_token
    try:
        if user_device.platform == 'IOS':
            device = APNSDevice.objects.get(registration_id=user_token)
            message = "Your Order %s is ready for collection"% order_id
            device.send_message(message, badge=1, sound="default", extra={"orderId":"%s" % order_id})
        elif user_device.platform == 'ANDROID':
            device = GCMDevice.objects.get(registration_id=user_token)
            message = "Your Order %s is ready for collection"% order_id
            device.send_message(message, extra={"orderId":"%s" % order_id})


        print "Sent Badge Msg & Custom"     
        return True
    except Exception as e:
#        print 'oops %s (%s)' % (e.message, type(e))
        print 'oops %s ' % e
        return False

#    device = APNSDevice.objects.get(registration_id=user_token)
#    print "Got a device %s" % device
#    device.send_message("You've got mail") 
    # Alert message may only be sent as text.
#    device.send_message(None, badge=5) # No alerts but with badge.
#    device.send_message(None, badge=1, extra={"foo": "bar"}) # Silent message with badge and added custom data.          