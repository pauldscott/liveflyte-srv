from django.db import models
from django.contrib.auth.models import User

# Create your models here.


# UserProfile - every user of the system has a user_profile record   
class UserProfile(models.Model):
    user = models.OneToOneField(User)
    user_group = models.ForeignKey('UserGroup', null=True, blank=True, default = None)
    first_name = models.CharField(max_length=40, null = True, blank = True) 
    last_name = models.CharField(max_length=40, null = True, blank = True) 
    access_token = models.CharField(max_length=300, null = True, blank = True)
    liveflyte_registered = models.BooleanField()
    registration_token = models.CharField(max_length=40, null = True, blank = True) 
    password_token = models.CharField(max_length=40, null = True, blank = True)
#     user_id = models.BigIntegerField(null = True, blank = True)
#     venue_id = models.BigIntegerField(null = True, blank = True, default = None)   
#     venue_name = models.CharField(max_length=40, null = True, blank = True)   
    company  = models.ForeignKey('Company')
    address = models.CharField(max_length=40, null = True, blank = True) 
    town_city = models.CharField(max_length=40, null = True, blank = True) 
    county_state = models.CharField(max_length=40, null = True, blank = True) 
    post_zipcode = models.CharField(max_length=40, null = True, blank = True) 
#     country_id = models.ForeignKey('Country', null=True, blank=True, default = None)
    country_id = models.BigIntegerField(null = True, blank = True)	
    date_of_birth = models.DateField(null=True, blank=True)
    tour = models.CharField(max_length=1, null = True, blank = True) 
    insert_point = models.DateTimeField( blank=True, null=True, default=None)
    update_point = models.DateTimeField( blank=True, null=True, default=None)
    
    def __unicode__(self):
        print self.user
        return self.user.username    
    
    def save(self, *args, **kwargs):
        super(UserProfile, self).save(*args, **kwargs)
    
    class Meta:
        verbose_name_plural = "userProfiles" 

# Any company/organisation in the system needs a company record, companies are of
# different types, e.g. Client, Operator, National Aviation Authority
class Company(models.Model):
    company_name = models.CharField(max_length=40, null = True, blank = True) 
    address1 = models.CharField(max_length=40, null = True, blank = True) 
    town_city = models.CharField(max_length=40, null = True, blank = True) 
    county_state = models.CharField(max_length=40, null = True, blank = True) 
    post_zipcode = models.CharField(max_length=40, null = True, blank = True) 
    country_id = models.ForeignKey('Country')
    phone = models.CharField(max_length=40, null = True, blank = True)
    email = models.CharField(max_length=40, null = True, blank = True)
    website = models.CharField(max_length=40, null = True, blank = True)
    person = models.ForeignKey('Person')
    contact_phone = models.CharField(max_length=40, null = True, blank = True)
    contact_email = models.CharField(max_length=40, null = True, blank = True)
    contact_title = models.CharField(max_length=40, null = True, blank = True)
    company_type = models.ForeignKey('CompanyType', null=True, blank=True, default = None)
#     paypal_acc = models.CharField(max_length=40, null = True, blank = True)
#     latitude = models.FloatField( null = True, blank = True)
#     longitude = models.FloatField( null = True, blank = True)
    insert_point = models.DateTimeField( blank=True, null=True, default=None)
    update_point = models.DateTimeField( blank=True, null=True, default=None)
    state_indicator = models.IntegerField(blank=True, null=True, default=0)
  
    def __unicode__(self):
        return self.company_name  
    
    class Meta:
        verbose_name_plural = "companies"    

# Aircraft lists all the aircraft registered to fly against a company
class AirCraft(models.Model):
    aircraft_name = models.CharField(max_length=40, null = True, blank = True) 
    company  = models.ForeignKey('Company')
    serial_no = models.CharField(max_length=20, null = True, blank = True)
    
    uas_name = models.CharField(max_length=20, null = True, blank = True)
    registration_no = models.CharField(max_length=20, null = True, blank = True)
    wingspan = models.DecimalField( null = False, default=0.00, blank = True, max_digits=5, decimal_places=2)
    length = models.DecimalField( null = False, default=0.00, blank = True, max_digits=5, decimal_places=2)
    mtom = models.DecimalField( null = False, default=0.00, blank = True, max_digits=5, decimal_places=2)
    no_engines = models.IntegerField(blank=True, null=True, default=0)
    engine_capacity = models.IntegerField(blank=True, null=True, default=0)
    ctrl_frequency = models.DecimalField( null = False, default=0.00, blank = True, max_digits=5, decimal_places=2)
    registered_date = models.DateTimeField( blank=True)
    insert_point = models.DateTimeField( blank=True, null=True, default=None)
    update_point = models.DateTimeField( blank=True, null=True, default=None)
    state_indicator = models.IntegerField(blank=True, null=True, default=0)

    class Meta:
        verbose_name_plural = "aircrafts"    
    
    def __unicode__(self):
        return self.aircraft_name

# Battery lists all batteries that are registered against a company
class Battery(models.Model):
    battery_id = models.CharField(max_length=40, null = True, blank = True) 
    company  = models.ForeignKey('Company')
    serial_no = models.CharField(max_length=100, null = True, blank = True)
    description = models.CharField(max_length=200, null = True, blank = True)
    capacity = models.IntegerField(blank=True, null=True, default=0)
    cells = models.IntegerField(blank=True, null=True, default=0)
    registered_date = models.DateTimeField( blank=True)
    insert_point = models.DateTimeField( blank=True, null=True, default=None)
    update_point = models.DateTimeField( blank=True, null=True, default=None)
    state_indicator = models.IntegerField(blank=True, null=True, default=0)

    class Meta:
        verbose_name_plural = "batteries"    
    
    def __unicode__(self):
        return "%s" % self.battery_id

# FlightBattery lists all batteries that are registered against a company
class FlightBattery(models.Model):
    flight = models.ForeignKey('Flight')
    battery = models.ManyToManyField('Battery')
    insert_point = models.DateTimeField( blank=True, null=True, default=None)
    update_point = models.DateTimeField( blank=True, null=True, default=None)
    state_indicator = models.IntegerField(blank=True, null=True, default=0)

    class Meta:
        verbose_name_plural = "flightBatteries"    
    
    def __unicode__(self):
        return "%s" % self.flight.flight_no
 
# Company type will be either Client, Operator, NAA, etc.
class CompanyType(models.Model):
    company_type = models.CharField(max_length=20, null = True, blank = True) 
    description = models.CharField(max_length=40, null = True, blank = True)
    state_indicator = models.IntegerField(blank=True, null=True, default=0)

    class Meta:
        verbose_name_plural = "company types"    
    
    def __unicode__(self):
        return self.company_type

# Jobs are pre-booked pieces of work for a client against which all checks and flights are recorded    
class Job(models.Model):
    job_no = models.CharField(max_length=10, null = True, blank = True) 
    description = models.CharField(max_length=40, null = True, blank = True)
#     operator  = models.ForeignKey('Company')
    customer  = models.ForeignKey('Company') 
    site = models.ForeignKey('Site')
    job_date = models.DateTimeField( blank=True, null=True, default=None)
    preflight_survey  = models.ForeignKey('PreFlightSurvey')
    onsite_survey  = models.ForeignKey('OnSiteSurvey')
    insert_point = models.DateTimeField( blank=True, null=True, default=None)
    state_indicator = models.IntegerField(blank=True, null=True, default=0)

    class Meta:
        verbose_name_plural = "jobs"    
    
    def __unicode__(self):
        return self.job_no

# Sites are locations where flights are to be carried out    
class Site(models.Model):
    name = models.CharField(max_length=10, null = True, blank = True) 
    description = models.CharField(max_length=40, null = True, blank = True)
    latitude = models.FloatField( null = True, blank = True)
    longitude = models.FloatField( null = True, blank = True)
    altitude = models.FloatField( null = True, blank = True)
    address1 = models.CharField(max_length=40, null = True, blank = True) 
    town_city = models.CharField(max_length=40, null = True, blank = True) 
    county_state = models.CharField(max_length=40, null = True, blank = True) 
    post_zipcode = models.CharField(max_length=40, null = True, blank = True) 
    country_id = models.ForeignKey('Country')
    insert_point = models.DateTimeField( blank=True, null=True, default=None)
    state_indicator = models.IntegerField(blank=True, null=True, default=0)

    class Meta:
        verbose_name_plural = "sites"    
    
    def __unicode__(self):
        return self.job_no


# All Logged flights for a particular pilot and aircraft        
class Flight(models.Model):
    flight_no = models.IntegerField(blank=True, null=True, default=0)
    description = models.CharField(max_length=40, null = True, blank = True)
    company  = models.ForeignKey('Company')
    craft = models.ForeignKey('AirCraft', related_name='craft',blank=True, null=True, default=None)
    flight_date = models.DateTimeField( blank=True, null=True, default=None)
    pic  = models.ForeignKey('Staff', related_name='pic',blank=True, null=True, default=None)
    observer = models.ForeignKey('Staff', related_name='observer',blank=True, null=True, default=None)
    payload_operator = models.ForeignKey('Staff', related_name='payload_operator',blank=True, null=True, default=None)
    spotter = models.ForeignKey('Staff', related_name='spotter',blank=True, null=True, default=None)
    latitude = models.FloatField( null = True, blank = True)
    longitude = models.FloatField( null = True, blank = True)
    start_flight = models.DateTimeField( blank=True, null=True, default=None)
    end_flight = models.DateTimeField( blank=True, null=True, default=None)
    insert_point = models.DateTimeField( blank=True, null=True, default=None)
    update_point = models.DateTimeField( blank=True, null=True, default=None)
    state_indicator = models.IntegerField(blank=True, null=True, default=0)

    class Meta:
        verbose_name_plural = "flights"    
    
    def __unicode__(self):
        return '%s' % self.flight_no

class Log(models.Model):
    flight  = models.ForeignKey('Flight')
    log_no = models.IntegerField(blank=False, null=False, default=1) 
    altitude = models.FloatField( null = True, blank = True)
    latitude = models.FloatField( null = True, blank = True)
    longitude = models.FloatField( null = True, blank = True)
    insert_point = models.DateTimeField( blank=True, null=True, default=None)
    update_point = models.DateTimeField( blank=True, null=True, default=None)
    state_indicator = models.IntegerField(blank=True, null=True, default=0)

    class Meta:
        verbose_name_plural = "logs"    
    
#     def __unicode__(self):
#         return self.log_no + ':' + 
 
class BlackBox(models.Model):
    appid = models.CharField(max_length=13, null = True, blank = True)
    craft = models.OneToOneField('AirCraft', blank=True, null=True, default=None)
    insert_point = models.DateTimeField( blank=True, null=True, default=None)
    update_point = models.DateTimeField( blank=True, null=True, default=None)
    state_indicator = models.IntegerField(blank=True, null=True, default=0)

    class Meta:
        verbose_name_plural = "Blackboxes"    
    
    def __unicode__(self):
        return self.appid
     
      
# All members of the flight crew, spotters, camera operator, etc.        
class FlightCrew(models.Model):
    crew  = models.ForeignKey('Staff')
    state_indicator = models.IntegerField(blank=True, null=True, default=0)

    class Meta:
        verbose_name_plural = "flight crew"    
    
#    def __unicode__(self):
#        return self.flight_no

#### SURVEY's

# Presite Survey to be completed before flight       
class PreFlightSurvey(models.Model):
    survey_date = models.DateTimeField( blank=True, null=True, default=None)
    airspace = models.CharField(max_length=200, null = True, blank = True)
    terrain = models.CharField(max_length=200, null = True, blank = True)
    proximities = models.CharField(max_length=200, null = True, blank = True)
    hazards = models.CharField(max_length=200, null = True, blank = True)
    restrictions = models.CharField(max_length=200, null = True, blank = True)
    sensitivities = models.CharField(max_length=200, null = True, blank = True)
    people = models.CharField(max_length=200, null = True, blank = True)
    livestock = models.CharField(max_length=200, null = True, blank = True)
    permission = models.CharField(max_length=200, null = True, blank = True)
    access = models.CharField(max_length=200, null = True, blank = True)
    cordon = models.CharField(max_length=200, null = True, blank = True)
    footpaths = models.CharField(max_length=200, null = True, blank = True)
    alternate = models.CharField(max_length=200, null = True, blank = True)
    risk_mitigation = models.CharField(max_length=200, null = True, blank = True)
    weather = models.CharField(max_length=200, null = True, blank = True)
    notams = models.CharField(max_length=200, null = True, blank = True)
    local_atc_contact = models.CharField(max_length=20, null = True, blank = True)
    local_atc_contact_date = models.DateTimeField( blank=True, null=True, default=None)
    regional_atc_contact = models.CharField(max_length=20, null = True, blank = True)
    regional_atc_contact_date = models.DateTimeField( blank=True, null=True, default=None)
    military_control_conact = models.CharField(max_length=20, null = True, blank = True)
    military_control_conact_date = models.DateTimeField( blank=True, null=True, default=None)
    notice_to_airmen = models.DateTimeField( blank=True, null=True, default=None) 
    insert_point = models.DateTimeField( blank=True, null=True, default=None)
    update_point = models.DateTimeField( blank=True, null=True, default=None)
    state_indicator = models.IntegerField(blank=True, null=True, default=0)

    class Meta:
        verbose_name_plural = "pre-flight surveys"    
#     
#    def __unicode__(self):
#        return self.flight_no

# Onsite Survey to be completed before flight       
class OnSiteSurvey(models.Model):
    survey_date = models.DateTimeField( blank=True, null=True, default=None)
    wind_speed = models.FloatField( null = True, blank = True)
    wind_direction = models.CharField(max_length=20, null = True, blank = True)
    temperature = models.FloatField( null = True, blank = True)
    obstructions = models.CharField(max_length=200, null = True, blank = True)
    visual_limitations = models.CharField(max_length=200, null = True, blank = True)
    cordon = models.CharField(max_length=200, null = True, blank = True)
    livestock = models.CharField(max_length=200, null = True, blank = True)
    terrain = models.CharField(max_length=200, null = True, blank = True)
    permission = models.CharField(max_length=200, null = True, blank = True)
    public = models.CharField(max_length=200, null = True, blank = True)
    air_traffic = models.CharField(max_length=200, null = True, blank = True)
    communication = models.CharField(max_length=200, null = True, blank = True)
    proximity = models.CharField(max_length=200, null = True, blank = True)
    take_off_area = models.CharField(max_length=200, null = True, blank = True)
    landing_area = models.CharField(max_length=200, null = True, blank = True)
    operational_zone = models.CharField(max_length=200, null = True, blank = True)
    emergency_area = models.CharField(max_length=200, null = True, blank = True)
    local_police_telephone = models.CharField(max_length=10, null = True, blank = True)
    local_hospital_telephone = models.CharField(max_length=10, null = True, blank = True)
    local_atc_telephone = models.CharField(max_length=10, null = True, blank = True)
    notes = models.CharField(max_length=1000, null = True, blank = True)
    insert_point = models.DateTimeField( blank=True, null=True, default=None)
    update_point = models.DateTimeField( blank=True, null=True, default=None)
    state_indicator = models.IntegerField(blank=True, null=True, default=0)

    class Meta:
        verbose_name_plural = "on-site surveys"    
#     
#    def __unicode__(self):
#        return self.flight_no


class Person(models.Model):
    first_name = models.CharField(max_length=40, null = True, blank = True) 
    last_name = models.CharField(max_length=40, null = True, blank = True) 
    name = models.CharField(max_length=40, null = False, blank = False)
    email = models.CharField(max_length=40, null = True, blank = True) 
    address = models.CharField(max_length=40, null = True, blank = True) 
    town_city = models.CharField(max_length=40, null = True, blank = True) 
    county_state = models.CharField(max_length=40, null = True, blank = True) 
    post_zipcode = models.CharField(max_length=40, null = True, blank = True) 
    country_id = models.BigIntegerField(null = True, blank = True)	
    insert_point = models.DateTimeField( blank=True, null=True, default=None)
    update_point = models.DateTimeField( blank=True, null=True, default=None)
    
    def __unicode__(self):
        return self.first_name + ' ' + self.last_name    
    
    def save(self, *args, **kwargs):
        super(Person, self).save(*args, **kwargs)
    
    class Meta:
        verbose_name_plural = "people" 


    
# Lists the crew and pilots    
class Staff(models.Model):
    name = models.CharField(max_length=40, null = False, blank = False)
    company = models.ForeignKey('Company')
#     certification = models.ForeignKey('Certification') 
    role = models.ForeignKey('Role') 
    insert_point = models.DateTimeField( blank=True, null=True, default=None)
    update_point = models.DateTimeField( blank=True, null=True, default=None)
    state_indicator = models.IntegerField(blank=False, null=True, default=0)

    class Meta:
        verbose_name_plural = "members of staff"    

    def __unicode__(self):
        return self.name  

# Role identifies the different classes of people in an organisation/company
class Role(models.Model):
    title = models.CharField(max_length=40, null = False, blank = False)
    insert_point = models.DateTimeField( blank=True, null=True, default=None)
    update_point = models.DateTimeField( blank=True, null=True, default=None)
    state_indicator = models.IntegerField(blank=False, null=True, default=0)

    class Meta:
        verbose_name_plural = "roles"    

    def __unicode__(self):
        return self.title  


# class Certification(models.Model):
#     name = models.CharField(max_length=40, null = False, blank = False)
#     company = models.ForeignKey('Company')
#     certification = models.ForeignKey('Certification') 
#     company = models.ForeignKey('Company') 
#     insert_point = models.DateTimeField( blank=True, null=True, default=None)
#     update_point = models.DateTimeField( blank=True, null=True, default=None)
#     state_indicator = models.IntegerField(blank=False, null=True, default=0)
# 
#     class Meta:
#         verbose_name_plural = "pilots"    
# 
#     def __unicode__(self):
#         return self.name  
        
# class Menu(models.Model):
#     venue = models.OneToOneField(Venue)
#     menu = models.IntegerField(blank=True, null=True, default=0)
# 
#     def __unicode__(self):
#         return self.venue.venue_name  
   
    
# class Item(models.Model):
#     #venue_id = models.IntegerField(blank=True, null=False, default=None)
#     menu_id  = models.ForeignKey('Menu')
#     row  = models.IntegerField(blank=True, null=True)
#     index  = models.IntegerField(blank=True, null=True)
#     parent = models.CharField(max_length=40, null = True, blank = True)
#     parent_id  = models.CharField(max_length=10, null = True, blank = True)
#     category = models.CharField(max_length=100, null = True, blank = True)
#     linked_category = models.CharField(max_length=40, null = True, blank = True)
#     itemName = models.CharField(max_length=100, null = True, blank = True)
#     itemPrice = models.DecimalField( null = False, default=0.00, blank = True, max_digits=5, decimal_places=2)
#     itemAge = models.IntegerField(blank=True, null=True, default=0)
#     is_linked_category = models.IntegerField(blank=True, null=True, default=0)
#     state_indicator = models.IntegerField(blank=True, null=True, default=0)
#     
#    def __unicode__(self):
#        return self.parent + "-" +  self.category + "-" + self.itemName + "-" 
#
#     class Meta:
#         verbose_name_plural = "items"    
        
#class Menu(models.Model):
#    venue = models.OneToOneField(Venue)
#    menu = models.IntegerField(blank=True, null=True, default=0)
#
#    def __unicode__(self):
#        return self.venue.venue_name  

# class Feedback(models.Model):
#     user = models.OneToOneField(User)
#     comment = models.CharField(max_length=400, null = True, blank = True) 
#     comment_date = models.DateField(auto_now_add=True, blank=True)
#     state_indicator = models.IntegerField(blank=True, null=True, default=0)
#   
#     def __unicode__(self):
#         return self.model  
# 
# class PayPal(models.Model):
#     userid = models.CharField(max_length=100, null = True, blank = True) 
#     pwd = models.CharField(max_length=100, null = True, blank = True) 
#     sig = models.CharField(max_length=100, null = True, blank = True) 
#     platform = models.CharField(max_length=100, null = True, blank = True) 
#     dgGoodsUrl = models.CharField(max_length=100, null = True, blank = True)
#     app_id = models.CharField(max_length=100, null = True, blank = True) 
#     email = models.CharField(max_length=100, null = True, blank = True) 
#   
#     def __unicode__(self):
#        return self.userid  
#     
#     class Meta:
#         verbose_name_plural = "PayPal Settings"    
   
class User_Device(models.Model):
#    user = models.OneToOneField('UserProfile')
    user = models.ForeignKey('UserProfile')
    model = models.IntegerField(blank=True, null=True)
    notification_token = models.CharField(max_length=255, null=True)
    platform = models.CharField(max_length=40, null = True, blank = True) 
    uuid = models.CharField(max_length=40, null = True, blank = True) 
    version = models.CharField(max_length=40, null = True, blank = True) 
  
#    def __unicode__(self):
#        return self.user.user.username  
    
class UserGroup(models.Model):
    name = models.CharField(max_length=40, null = True, blank = True) 
  
    def __unicode__(self):
        return self.name  
    

class Country(models.Model):
    name = models.CharField(max_length=40, null = True, blank = True) 
    iso_2 = models.CharField(max_length=2, null = True, blank = True)
    iso_3 = models.CharField(max_length=3, null = True, blank = True)
    dial_prefix = models.CharField(max_length=3, null = True, blank = True)
    currency_code = models.CharField(max_length=3, null = True, blank = True)
    currency_symbol = models.CharField(max_length=1, null = True, blank = True)

    def __unicode__(self):
        return self.name  

    class Meta:
        verbose_name_plural = "countries"    

class Request(models.Model):
    user = models.OneToOneField('UserProfile')
