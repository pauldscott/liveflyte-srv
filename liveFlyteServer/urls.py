from django.conf.urls import patterns, include, url, handler404
from django.views.generic.simple import redirect_to

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

handler404 = 'api.views.my_custom_404_view'


urlpatterns = patterns('',
    # Examples:
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^restframework', include('djangorestframework.urls', namespace='djangorestframework')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include('api.urls')), 
    (r'^$', redirect_to, {'url': 'site/index.html'})

)
